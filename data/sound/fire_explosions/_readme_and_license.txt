Sound pack downloaded from Freesound.org
----------------------------------------

This pack of sounds contains sounds by the following user:
 - LiamG_SFX ( https://www.freesound.org/people/LiamG_SFX/ )

You can find this pack online at: https://www.freesound.org/people/LiamG_SFX/packs/18075/

License details
---------------

Attribution: http://creativecommons.org/licenses/by/3.0/
Attribution Noncommercial: http://creativecommons.org/licenses/by-nc/3.0/


Sounds in this pack
-------------------

  * 334236__liamg-sfx__fireball-flyby-1b.wav
    * url: https://www.freesound.org/s/334236/
    * license: Attribution
  * 334235__liamg-sfx__fireball-flyby-1.wav
    * url: https://www.freesound.org/s/334235/
    * license: Attribution
  * 334234__liamg-sfx__fireball-cast-1.wav
    * url: https://www.freesound.org/s/334234/
    * license: Attribution
  * 323086__liamg-sfx__meteor-fly-by-impact.wav
    * url: https://www.freesound.org/s/323086/
    * license: Attribution
  * 323085__liamg-sfx__meteor-fly-by-1.wav
    * url: https://www.freesound.org/s/323085/
    * license: Attribution
  * 322512__liamg-sfx__explosion-15b.wav
    * url: https://www.freesound.org/s/322512/
    * license: Attribution Noncommercial
  * 322511__liamg-sfx__explosion-1b.wav
    * url: https://www.freesound.org/s/322511/
    * license: Attribution Noncommercial
  * 322510__liamg-sfx__explosion-1c.wav
    * url: https://www.freesound.org/s/322510/
    * license: Attribution Noncommercial
  * 322509__liamg-sfx__explosion-2.wav
    * url: https://www.freesound.org/s/322509/
    * license: Attribution Noncommercial
  * 322508__liamg-sfx__explosion-2.wav
    * url: https://www.freesound.org/s/322508/
    * license: Attribution Noncommercial
  * 322507__liamg-sfx__explosion-5.wav
    * url: https://www.freesound.org/s/322507/
    * license: Attribution Noncommercial
  * 322506__liamg-sfx__explosion-6-7-8-layered.wav
    * url: https://www.freesound.org/s/322506/
    * license: Attribution Noncommercial
  * 322505__liamg-sfx__explosion-6.wav
    * url: https://www.freesound.org/s/322505/
    * license: Attribution Noncommercial
  * 322504__liamg-sfx__explosion-6a.wav
    * url: https://www.freesound.org/s/322504/
    * license: Attribution Noncommercial
  * 322503__liamg-sfx__explosion-6b.wav
    * url: https://www.freesound.org/s/322503/
    * license: Attribution Noncommercial
  * 322502__liamg-sfx__explosion-15.wav
    * url: https://www.freesound.org/s/322502/
    * license: Attribution Noncommercial
  * 322501__liamg-sfx__explosion-14c.wav
    * url: https://www.freesound.org/s/322501/
    * license: Attribution Noncommercial
  * 322500__liamg-sfx__explosion-14b.wav
    * url: https://www.freesound.org/s/322500/
    * license: Attribution Noncommercial
  * 322499__liamg-sfx__explosion-14a.wav
    * url: https://www.freesound.org/s/322499/
    * license: Attribution Noncommercial
  * 322498__liamg-sfx__explosion-13d.wav
    * url: https://www.freesound.org/s/322498/
    * license: Attribution Noncommercial
  * 322497__liamg-sfx__explosion-13c.wav
    * url: https://www.freesound.org/s/322497/
    * license: Attribution Noncommercial
  * 322496__liamg-sfx__explosion-13b.wav
    * url: https://www.freesound.org/s/322496/
    * license: Attribution Noncommercial
  * 322495__liamg-sfx__explosion-13.wav
    * url: https://www.freesound.org/s/322495/
    * license: Attribution Noncommercial
  * 322494__liamg-sfx__explosion-12.wav
    * url: https://www.freesound.org/s/322494/
    * license: Attribution Noncommercial
  * 322493__liamg-sfx__explosion-11e.wav
    * url: https://www.freesound.org/s/322493/
    * license: Attribution Noncommercial
  * 322492__liamg-sfx__explosion-7b.wav
    * url: https://www.freesound.org/s/322492/
    * license: Attribution Noncommercial
  * 322491__liamg-sfx__explosion-7a.wav
    * url: https://www.freesound.org/s/322491/
    * license: Attribution Noncommercial
  * 322490__liamg-sfx__explosion-7d.wav
    * url: https://www.freesound.org/s/322490/
    * license: Attribution Noncommercial
  * 322489__liamg-sfx__explosion-7c.wav
    * url: https://www.freesound.org/s/322489/
    * license: Attribution Noncommercial
  * 322488__liamg-sfx__explosion-8.wav
    * url: https://www.freesound.org/s/322488/
    * license: Attribution Noncommercial
  * 322487__liamg-sfx__explosion-7e.wav
    * url: https://www.freesound.org/s/322487/
    * license: Attribution Noncommercial
  * 322486__liamg-sfx__explosion-9.wav
    * url: https://www.freesound.org/s/322486/
    * license: Attribution Noncommercial
  * 322485__liamg-sfx__explosion-11.wav
    * url: https://www.freesound.org/s/322485/
    * license: Attribution Noncommercial
  * 322484__liamg-sfx__explosion-11b.wav
    * url: https://www.freesound.org/s/322484/
    * license: Attribution Noncommercial
  * 322483__liamg-sfx__explosion-10.wav
    * url: https://www.freesound.org/s/322483/
    * license: Attribution Noncommercial
  * 322482__liamg-sfx__explosion-10b.wav
    * url: https://www.freesound.org/s/322482/
    * license: Attribution Noncommercial
  * 322481__liamg-sfx__explosion-1.wav
    * url: https://www.freesound.org/s/322481/
    * license: Attribution Noncommercial
  * 322480__liamg-sfx__explosion-11c.wav
    * url: https://www.freesound.org/s/322480/
    * license: Attribution Noncommercial
  * 322479__liamg-sfx__explosion-11d.wav
    * url: https://www.freesound.org/s/322479/
    * license: Attribution Noncommercial
  * 322176__liamg-sfx__background-fire.wav
    * url: https://www.freesound.org/s/322176/
    * license: Attribution Noncommercial


