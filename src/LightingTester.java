import eelengine.*;
import eelengine.rendering.VisEngine;
import processing.core.PApplet;
import processing.opengl.PJOGL;

import eelengine.Point;
/**
 * Created by benja on 10/4/2017.
 */
public class LightingTester extends PApplet {
    public void settings(){
        // I believe:
        // negative is primary monitor
        // 0 is all monitors (woo!)
        // positive is specific monitor, no idea how it's decided. 1 isn't always primary
        if(false)fullScreen(P2D,-2);
        else
        {
            size(1800,900,P2D);
        }
        smooth(8);
        //surface.setIcon(icon);
        PJOGL.setIcon("data/BlueLogo2(007fff).png");
    }

    VisEngine visEngine=new VisEngine();
    int part=0;
    Point p1=null;
    Point p2=null;
    Point p3=null;
    Point p4=null;
    public void setup() {
        frameRate(1000);

        if(true){
            visEngine.addLine(new Point(119, 86), new Point(106, 144));
            visEngine.addLine(new Point(106, 144), new Point(184, 153));
            visEngine.addLine(new Point(184, 153), new Point(189, 91));
            visEngine.addLine(new Point(189, 91), new Point(119, 86));
            visEngine.addLine(new Point(340, 242), new Point(264, 404));
            visEngine.addLine(new Point(264, 404), new Point(465, 494));
            visEngine.addLine(new Point(465, 494), new Point(511, 341));
            visEngine.addLine(new Point(511, 341), new Point(340, 242));
            visEngine.addLine(new Point(197, 456), new Point(403, 399));
            visEngine.addLine(new Point(403, 399), new Point(490, 571));
            visEngine.addLine(new Point(490, 571), new Point(243, 618));
            visEngine.addLine(new Point(243, 618), new Point(197, 456));
            visEngine.addLine(new Point(323, 575), new Point(369, 798));
            visEngine.addLine(new Point(369, 798), new Point(445, 803));
            visEngine.addLine(new Point(445, 803), new Point(406, 553));
            visEngine.addLine(new Point(406, 553), new Point(323, 575));
            visEngine.addLine(new Point(585, 84), new Point(447, 111));
            visEngine.addLine(new Point(447, 111), new Point(481, 229));
            visEngine.addLine(new Point(481, 229), new Point(614, 194));
            visEngine.addLine(new Point(614, 194), new Point(585, 84));
            visEngine.addLine(new Point(916, 188), new Point(868, 311));
            visEngine.addLine(new Point(868, 311), new Point(986, 317));
            visEngine.addLine(new Point(986, 317), new Point(1017, 192));
            visEngine.addLine(new Point(1017, 192), new Point(916, 188));
            visEngine.addLine(new Point(721, 511), new Point(772, 620));
            visEngine.addLine(new Point(772, 620), new Point(888, 586));
            visEngine.addLine(new Point(888, 586), new Point(867, 492));
            visEngine.addLine(new Point(867, 492), new Point(721, 511));
            visEngine.addLine(new Point(1327, 318), new Point(1304, 422));
            visEngine.addLine(new Point(1304, 422), new Point(1417, 445));
            visEngine.addLine(new Point(1417, 445), new Point(1449, 345));
            visEngine.addLine(new Point(1449, 345), new Point(1327, 318));
            visEngine.addLine(new Point(1358, 582), new Point(1374, 840));
            visEngine.addLine(new Point(1374, 840), new Point(1318, 857));
            visEngine.addLine(new Point(1318, 857), new Point(1435, 618));
            visEngine.addLine(new Point(1435, 618), new Point(1358, 582));
            visEngine.addLine(new Point(1377, 55), new Point(1173, 179));
            visEngine.addLine(new Point(1173, 179), new Point(1469, 127));
            visEngine.addLine(new Point(1469, 127), new Point(1474, 50));
            visEngine.addLine(new Point(1474, 50), new Point(1377, 55));
            visEngine.addLine(new Point(1021, 208), new Point(1016, 250));
            visEngine.addLine(new Point(1016, 250), new Point(1056, 253));
            visEngine.addLine(new Point(1056, 253), new Point(1065, 216));
            visEngine.addLine(new Point(1065, 216), new Point(1021, 208));
            visEngine.addLine(new Point(984, 330), new Point(974, 367));
            visEngine.addLine(new Point(974, 367), new Point(1014, 376));
            visEngine.addLine(new Point(1014, 376), new Point(1024, 338));
            visEngine.addLine(new Point(1024, 338), new Point(984, 330));
        }
    }

    public void line(Point a,Point b){line((float)a.x,(float)a.y,(float)b.x,(float)b.y);}
    public void draw(){
        if(frameCount%50==0)println(frameRate);
        background(0);
        Point a=new Point(100,100);
        Point b=new Point(mouseX,mouseY);
//        Point c=new Point(100,100);
//        Point d=new Point(100,100);
//        println(a.equals(b));
        stroke(255,255,0);
//        if(Util.intersectInclusive(a,b,c,d))stroke(0,255,0)
        //line(a,b);
        stroke(0,255,255);
        line(a,Util.lineInDirection(a,50,Util.direction(a,b)));
//        line(c.x,c.y,d.x,d.y);
//        line(1000,500,1010,510);
        stroke(255,0,0);

        for(int i=0;i<visEngine.getIndA().size();i++){
            line(visEngine.getPoints().get(visEngine.getIndA().get(i)),visEngine.getPoints().get(visEngine.getIndB().get(i)));
        }
        visEngine.drawCasts(this,mouseX,mouseY,0,0,width,height);
//        visEngine.drawCasts(this,mouseX+10,mouseY,0,0,width,height);
//        visEngine.drawCasts(this,mouseX-10,mouseY,0,0,width,height);
//        visEngine.drawCasts(this,mouseX,mouseY+10,0,0,width,height);
//        visEngine.drawCasts(this,mouseX,mouseY-10,0,0,width,height);
//        Point c= new Point(width/2,height/2);
//        Point d=new eelengine.Point(mouseX,mouseY);
//        stroke(0,255,255);
//        if(Util.intersectInclusive(a,b,c,d))stroke(0,255,0);
//        line(c,d);
//        Point intersect=Util.intersectPoint(c,d,a,b);
//        ellipse((float)intersect.x,(float)intersect.y,20,20);
    }
    public void mousePressed(){
        if(true)return; // nah m8
        switch (part){
            case 0:
                p1=new Point(mouseX, mouseY);
                break;
            case 1:
                p2=new Point(mouseX, mouseY);
                break;
            case 2:
                p3=new Point(mouseX, mouseY);
                break;
            case 3:
                p4=new Point(mouseX, mouseY);
                visEngine.addLine(p1,p2);
                visEngine.addLine(p2,p3);
                visEngine.addLine(p3,p4);
                visEngine.addLine(p4,p1);
                break;
        }
        part++;
        part=part%4;
    }
    public void keyPressed() {
//        for (int i = 0; i < visEngine.indA.size(); i++) {
//            System.out.printf("visEngine.addLine(new Point(%d,%d),new Point(%d,%d));\n",
//                    visEngine.points.get(visEngine.indA.get(i)).x,
//                    visEngine.points.get(visEngine.indA.get(i)).y,
//                    visEngine.points.get(visEngine.indB.get(i)).x,
//                    visEngine.points.get(visEngine.indB.get(i)).y);
//        }
    }
    public static void main(String... args){
        PApplet.main("LightingTester");
    }

}
