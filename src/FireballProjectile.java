import eelengine.Damageable;
import eelengine.GameObject;
import eelengine.Projectile;
import org.dyn4j.geometry.Vector2;
import processing.core.PApplet;

/**
 * Created by Benjamin on 6/16/2017.
 */
public class FireballProjectile extends Projectile {

    public FireballProjectile(PApplet p, float x, float y, Vector2 velocity, int team) {
        super(p, x, y, velocity, team);
    }

    public FireballProjectile(PApplet p, Vector2 position, Vector2 velocity, int team) {
        super(p, position, velocity, team);
    }

    @Override
    protected void onProjectileHit(GameObject gameObject) {
        if(gameObject instanceof Damageable){
            ((Damageable)gameObject).addDamage(5,Damageable.STANDARD);
            kill();
        }

    }
}
