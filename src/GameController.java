import eelengine.Character;
import eelengine.physics.DriverPawn;
import eelengine.navigation.KeyBinder;
import eelengine.navigation.NavPath;
import eelengine.navigation.Node;
import eelengine.physics.Physics;
import eelengine.rendering.Renderer;
import eelengine.rendering.ViewController;
import org.dyn4j.collision.AxisAlignedBounds;
import org.dyn4j.collision.CategoryFilter;
import org.dyn4j.dynamics.Body;
import org.dyn4j.dynamics.RaycastResult;
import org.dyn4j.geometry.*;
import processing.core.PApplet;
import processing.core.PConstants;
import processing.core.PImage;
import eelengine.*;

import java.util.ArrayList;

/**
 * Created by Benjamin Welsh on 4/13/2017.
 * The primary game controller. Handles administration of like everything.
 */
public class GameController {
    PApplet p;
    GameWorld gameWorld =new GameWorld(100,100,"map2");
    DriverPawn playerPawn;
    Character playerCharacter;
    AICharacter spook;
    NavPath testPath;
    Vector2 testVector;
    PImage ground;
    private PImage icon_navEdit, icon_worldEdit;

    long last;

    boolean shift=false; //VERY TEMP
    boolean debugPanel=true;
    boolean devView=true;
    boolean showPhysics=true;

    boolean navEdit=false;
    boolean worldEdit=false;
    private float selectionStartX,selectionStartY;
    private boolean selecting=false;
    private float selectionPrecision=1f;

    ViewController viewController;
    private int in_forward=0;
    private int in_right=0;

    Sprite fireball;
    Projectile testProjectile;
    //Mouse

    KeyBinder keyBinder=new KeyBinder();

    public GameController(PApplet p) {
        this.p = p;
        viewController = new ViewController(p);

        icon_navEdit =p.loadImage("data/editor/icon_nav.png");
        icon_worldEdit =p.loadImage("data/editor/icon_world.png");
        ground=p.loadImage("data/maps/hearth_room.png");
        //world.setGravity(new Vector2(0.0D, 9.8D));
//        Rectangle floorRect = new Rectangle(15.0, 1.0);
//        floor=new Body();
//        floor.addFixture(new BodyFixture(floorRect));
//        floor.setMass(MassType.INFINITE);
//        floor.translate(0.0, -4.0);
//        gameWorld.world.addBody(floor);
//        Circle cirShape = new Circle(0.5);
//        circle=new Body();
//        circle.addFixture(cirShape);
//        circle.setMass(MassType.NORMAL);
//        circle.translate(2.0, 2.0);
//        // test adding some force
//        circle.applyForce(new Vector2(-100.0, 0.0));
//        // set some linear damping to simulate rolling friction
//        circle.setLinearDamping(0.05);
        //this.world.addBody(circle);
//        for(int i=0;i<70;i+=2)
//            bobs.add(new GameObject(p,2,i));
//        for(int i=0;i<70;i+=2)
//            bobs.add(new GameObject(p,4,i));
//        for(int i=0;i<70;i+=2)
//            bobs.add(new GameObject(p,6,i));
//        for(int i=0;i<70;i+=2)
//            bobs.add(new GameObject(p,-2,i));
//        for(int i=0;i<70;i+=2)
//            bobs.add(new GameObject(p,0,i));
//        for(GameObject bob:bobs)
//            this.world.addBody(bob.getBody());

        //temp test keybinds

        keyBinder.add(new KeyBinder.Binding(66){
            @Override
            public void down() {
                System.out.println("Down 1");
            }

            @Override
            public void up() {
                System.out.println("Up 1");
            }
        });

        fireball=new Sprite(p,"data/Fireball1.png","data/Fireball2.png","data/Fireball3.png","data/Fireball4.png");
        fireball.startAnimation(120);
        playerPawn =new DriverPawn(0,0,1);
        playerCharacter=new Character(p,playerPawn);
        gameWorld.spawnObject(playerCharacter);
        spook=new AICharacter(p, gameWorld.navigation, new DriverPawn(0,12,2));
        gameWorld.spawnObject(spook);
        this.last = System.nanoTime();
    }


    public void tick(){
        EngineProfilerTool.stampIn("tick");
//        p.println(circle.getTransform().getTranslation());
        // update the World
        playerPawn.setMoveVector(in_right,in_forward);
        // get the current time
        long time = System.nanoTime();
        // get the elapsed time from the last iteration
        long diff = time - this.last;
        // set the last time
        this.last = time;
        // convert from nanoseconds to seconds
        double elapsedTime = diff / 1.0e9;
        // update the world with the elapsed time
        gameWorld.physicsStep(elapsedTime);
        //p.delay(16);

        // GRAPHIX
        p.background(0);

        // Move view
        viewController.updatePan(p.mouseX-p.pmouseX,p.mouseY-p.pmouseY);

//        p.fill(255,0,0,100);
//        p.stroke(255,0,0);
        //p.ellipse(toScreenX(2),toScreenY(2),20,20);
        //p.rect(toScreenX(0),toScreenY(0),toScreenX(1),toScreenY(1));

        //Grid
        if(devView)drawGrid(true);

        // Center screen
        p.pushMatrix();
        p.translate(p.width/2,p.height/2);

        // World View
        p.pushMatrix();
        viewController.applyTransform(p);
        //19x23
        if(shift)
          for(int i=0;i<10;i++)
              for(int j=0;j<10;j++)
                  for(int q=0;q<5;q++)
                    p.image(ground,(i*1900),j*2400, 1900,-2400);
              //p.image(fireball.current(),(i*32)%2048,32*(i>>9));
        gameWorld.drawNav(p);
        if(testPath!=null)testPath.draw(p.g);
//        //p.rect(0,0,100,100);
//
//        p.image(fireball.current(),0,0);
//        playerCharacter.draw(p.g);
//        spook.draw(p.g);
        spook.AITick();

        gameWorld.renderStep(p.g);
        if(showPhysics||worldEdit) {
            gameWorld.drawWalls(p);

            debugdraw(playerPawn.getBody());
            debugdraw(playerPawn.controller);
        }
        //playerPawn.draw();
        p.popMatrix();
        // End World View

        // Center screen
        if(devView) {
            // Crosshair
            p.strokeWeight(1);
            p.stroke(255, 255, 0,50);
            p.line(-p.width / 2, 0, p.width / 2, 0);
            p.line(0, -p.height / 2, 0, p.height / 2);
        }
        p.popMatrix();
        //End Center screen

        if(testVector!=null){
            p.stroke(255,255,0);
            testVector.normalize();
            testVector.multiply(30);
            p.line(p.mouseX,p.mouseY,p.mouseX+(float)testVector.x,p.mouseY-(float)testVector.y);
        }

        p.stroke(100,100,255);
        p.fill(100,100,255,50);
        if(selecting){
            p.rectMode(PConstants.CORNERS);
            //p.rect(toScreenX(selectionStartX),toScreenY(selectionStartY),p.mouseX,p.mouseY);
            float endX=toScreenX(Util.round(toWorldX(p.mouseX),selectionPrecision));
            float endY=toScreenY(Util.round(toWorldY(p.mouseY),selectionPrecision));
            p.rect(toScreenX(selectionStartX),toScreenY(selectionStartY),endX,endY);
            p.rectMode(PConstants.CORNER);
        }

        if(devView) {
            p.stroke(0, 255, 255);
            p.noFill();
            {
                AxisAlignedBounds bounds = (AxisAlignedBounds)gameWorld.world.getBounds();
                editorBox(-bounds.getWidth()/2, -bounds.getHeight()/2, bounds.getWidth()/2, bounds.getHeight()/2, "Physics Domain", false);
            }
            p.fill(255);
//            p.textAlign(p.RIGHT);
            p.textSize(18);
            p.text(String.format("sX: %d  sY: %d",p.mouseX,p.mouseY),0,p.height-22);
            p.text(String.format("wX: %.2f  wY: %.2f",toWorldX(p.mouseX),toWorldY(p.mouseY)),0,p.height-4);
//            p.textAlign(p.LEFT);
        }
        if(debugPanel)drawDebugPanel();
        if(worldEdit)p.image(icon_worldEdit,p.width- icon_worldEdit.width,0);
        if(navEdit)p.image(icon_navEdit,p.width- icon_navEdit.width,0);
        gameWorld.cleanObjects();
        EngineProfilerTool.stampOut("tick");
        if(debugPanel)EngineProfilerTool.drawMeter(p.g,0,p.height/2);
        EngineProfilerTool.reset();
    }

    public void testProjectile(){
        Vector2 v=new Vector2(toWorldX(p.mouseX),toWorldY(p.mouseY));
        v.subtract(playerPawn.getPosition());
        v.multiply(100);

        gameWorld.spawnObject(new FireballProjectile(p,playerPawn.getPosition(),v,1));
    }
    public void testDoRayTrace(){
        ArrayList<RaycastResult> results= new ArrayList<>();
        gameWorld.world.raycast( playerPawn.getPosition(),new Vector2(toWorldX(p.mouseX),toWorldY((p.mouseY))), new CategoryFilter(Physics.M_ALL,Physics.WALL|Physics.MOB),true,true,false,results);
        System.out.println("Raytrace results "+results.size());
        for(RaycastResult result:results){
            System.out.println(result.toString());
        }
    }

    private void drawGrid(boolean quarters){
        viewController.drawGrid(quarters);
//        float num=(quarters&&viewController.getZoomFactor()>0.35)?.25f:1.0f;
//        int startX=(int)Math.ceil(toWorldX(0));
//        int endX=(int)Math.ceil(toWorldX(p.width));
//        int endY=(int)Math.ceil(toWorldY(0));
//        int startY=(int)Math.ceil(toWorldY(p.height))-1;
////        p.println(startY,endY,toScreenY(-15));
//        for(float i=startX;i<=endX;i+=num){
//            if(i==0)p.stroke(0,255,0,127);
//            else if(i%16==0)p.stroke(255,127);
//            else if(i%4==0)p.stroke(170,127);
//            else if(i%1==0)p.stroke(90,127);
//            else p.stroke(35,127);
//            p.line(toScreenX(i),0,toScreenX(i),p.height);
//        }
//        for(float i=startY;i<=endY;i+=num){
//            if(i==0)p.stroke(255,0,0,127);
//            else if(i%16==0)p.stroke(255,127);
//            else if(i%4==0)p.stroke(170,127);
//            else if(i%1==0)p.stroke(90,127);
//            else p.stroke(35,127);
//            p.line(0,toScreenY(i),p.width,toScreenY(i));
//        }
    }
    private void drawGrid(){
        viewController.drawGrid(false);
    }

    private void drawDebugPanel(){
        p.fill(0,127);
        p.stroke(255);
        p.rect(0,0,200,p.height/2);
        p.fill(200);
        p.textSize(14);
        p.text("F4 to toggle",10,15);
        p.text("Zoom Level: "+viewController.zoomLevel,20,30);
        p.text("Zoom Factor: "+viewController.getZoomFactor(),20,45);
        p.text("View Grabbed: "+(viewController.isViewGrabbed()?"true":"false"),20,60);
        p.text("Center(WC): ("+toWorldX(900)+","+toWorldY(p.height/2)+")",20,75);
        p.text("Top Left(WC): ("+toWorldX(0)+","+toWorldY(0)+")",20,90);
        p.text("test "+toScreenX(0)+", "+toWorldX(toScreenX(5))+", "+toScreenX(toWorldX(-2)),20,105);
        p.text("View Disp X: "+viewController.viewDisplacementX,20,120);
        p.text("View Disp Y: "+viewController.viewDisplacementY,20,135);
        p.text("Move Forward: "+in_forward,20,150);
        p.text("Move Right: "+in_right,20,165);
        p.text("FPS: "+p.frameRate,20,180);
        p.text("Shift: "+shift,20,195);
    }


    /* eelengine.rendering.ViewController convenience functions
    * Prebuilt block. See eelengine.rendering.ViewController.java
    * */
    float toWorldX(float screenX){
        return viewController.toWorldX(screenX);
    }
    float toWorldY(float screenY){
        return viewController.toWorldY(screenY);
    }
    private float toScreenX(double worldX){
        return (float)viewController.toScreenX(worldX);
    }

    private float toScreenY(double worldY){
        return (float)viewController.toScreenY(worldY);
    }
    /*End convenience functions*/


    public void debugdraw(Body body){
        Renderer.debugdraw(p.g,body);
//        p.strokeWeight(1);
//        p.stroke(255,255,0);
//        p.fill(0,0,127,80);
//        p.pushMatrix();
//        p.translate(eelengine.Trans.x(body.getTransform().getTranslationX()),eelengine.Trans.y(body.getTransform().getTranslationY()));
//        p.rotate(eelengine.Trans.r(body.getTransform().getRotation()));
//        for(BodyFixture fixture:body.getFixtures()){
//            if(fixture.getShape() instanceof Polygon) {
//                Polygon convex = (Polygon) fixture.getShape();
//                Vector2[] verts = convex.getVertices();
//                p.beginShape();
//                for (Vector2 vert : verts) {
//                    p.vertex(eelengine.Trans.x(vert.x), eelengine.Trans.y(vert.y));
//                }
//                p.vertex(eelengine.Trans.x(verts[0].x), eelengine.Trans.y(verts[0].y));
//                p.endShape();
//            //Circle
//            }else if(fixture.getShape() instanceof Circle){
//                Circle convex=(Circle) fixture.getShape();
//                float r=(float)convex.getRadius();
//                p.ellipse(0,0,eelengine.Trans.n(r*2),eelengine.Trans.n(r*2));
//                p.line(0,0,eelengine.Trans.x(r),0);
//            }
//        }
//        p.popMatrix();
    }



    public void editorBox(double x1, double y1, double x2, double y2, String label, boolean dashed){
        float xMin=toWorldX(0);
        float xMax=toWorldX(p.width);
        float yMax=toWorldY(0);
        float yMin=toWorldY(p.height);
        // If box is horizontally present.
        //p.println(y2,yMin,yMax);
        p.rectMode(p.CORNERS);
        p.rect(toScreenX(x1),toScreenY(y1),toScreenX(x2),toScreenY(y2));
        int fillC=p.g.fillColor;
        p.fill(p.g.strokeColor);
        if(y2<yMax)p.text(label,toScreenX(Util.clamp(xMin,(float)x1,(float)x2)),toScreenY(y2)+12);
        else if(y1>yMin)p.text(label,toScreenX(Util.clamp(xMin,(float)x1,(float)x2)),toScreenY(y1)-4);
        else if(Util.in((float)x1,xMin,xMax))p.text(label,toScreenX(x1),toScreenY(Util.clamp(yMax,(float)y1,(float)y2))+12);
        p.fill(fillC);


    }


    /*------------------------------------------------------------------------*/
    /*------------------------------------------------------------------------*/
    /*---------------------------------Input----------------------------------*/
    /*------------------------------------------------------------------------*/
    /*------------------------------------------------------------------------*/
    public void mousePressed(){
        //Right to move overrides all else
        if(p.mouseButton==p.RIGHT) {
            viewController.setViewGrabbed(true);
        }
        else{
            if(navEdit){
                if(p.mouseButton==PConstants.CENTER){
                    Node nodeToRemove= gameWorld.navigation.getNodeAt(toWorldX(p.mouseX),toWorldY(p.mouseY));
                    if(nodeToRemove==null)return;
                    gameWorld.navigation.removeNode(nodeToRemove);
                } else {
                    selectionStartX = Math.round(toWorldX(p.mouseX));
                    selectionStartY = Math.round(toWorldY(p.mouseY));
                    selecting = true;
                    selectionPrecision=1.0f;
                }
            }else if(worldEdit){
                //TODO multiple modes
                if(p.mouseButton==PConstants.LEFT){
                    selectionStartX = Util.round(toWorldX(p.mouseX),.25f);
                    selectionStartY = Util.round(toWorldY(p.mouseY),.25f);
                    selecting = true;
                    selectionPrecision=.25f;
                }
            }
        }
    }

    public void mouseReleased(){
        // right to look overrides all else
        if(p.mouseButton==p.RIGHT){
            viewController.setViewGrabbed(false);
            return;
        }
        if(navEdit) {
            if (selecting) {
                // Do things
                int selectionEndX = Math.round(toWorldX(p.mouseX));
                int selectionEndY = Math.round(toWorldY(p.mouseY));
                if (selectionStartX == selectionEndX || selectionStartY == selectionEndY) {
                    //System.out.println("Finding path");
                    testPath = gameWorld.navigation.findPath(playerPawn.getX(), playerPawn.getY(), toWorldX(p.mouseX), toWorldY(p.mouseY));
                    if (testPath != null) testPath.print();
                } else {
                    boolean validRegion = gameWorld.navigation.testRegion((int) selectionStartX, (int) selectionStartY, selectionEndX, selectionEndY);
                    System.out.println("Is region empty:" + validRegion);
                    if (validRegion) {
                        System.out.println("Generating node");
                        gameWorld.navigation.makeNode((int) selectionStartX, (int) selectionStartY, selectionEndX, selectionEndY);
                    }
                }

            }
            selecting = false;
        }else if(worldEdit){
            if(selecting) {
                float selectionEndX = Util.round(toWorldX(p.mouseX), .25f);
                float selectionEndY = Util.round(toWorldY(p.mouseY), .25f);
                if (selectionStartX == selectionEndX || selectionStartY == selectionEndY) {
                    // =invalid selection (one dimension is 0)
                } else {
                    gameWorld.addWall(selectionStartX, selectionStartY, selectionEndX, selectionEndY,shift);
                }
                selecting = false;
            }
        }
    }

    public void mouseWheel(int dval){
        viewController.adjustZoom(dval);
        //1System.out.println("Zoom Level:"+zoomLevel+" Zoom Factor:"+getZoomFactor());
    }

    public void addMove(int forward, int right){
        //p.println(forward,right);
        in_forward= Util.clamp(in_forward+forward,-1,1);
        in_right= Util.clamp(in_right+right,-1,1);

    }
    public void toggleNavEdit(){
        navEdit=!navEdit;
        worldEdit=false;
        if(navEdit)p.cursor(PConstants.CROSS);
        else p.cursor(PConstants.ARROW);
    }
    public void toggleWorldEdit(){
        navEdit=false;
        worldEdit=!worldEdit;
        if(worldEdit)p.cursor(PConstants.CROSS);
        else p.cursor(PConstants.ARROW);
    }
    public void reloadNav(){
        gameWorld.reloadNav();
    }
}