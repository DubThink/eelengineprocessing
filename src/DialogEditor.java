/**
 * Created by benja on 2/27/2017.
 * use http://magiccards.info
 */
import eelengine.rendering.ViewController;
import processing.core.PApplet;
import processing.core.PFont;
import processing.event.MouseEvent;
import processing.opengl.PJOGL;

public class DialogEditor extends PApplet{

    ViewController viewController;
    //Button button=new Button(this,20,20,180,50,"lolol");

    public void settings(){
        // I believe:
        // negative is primary monitor
        // 0 is all monitors (woo!)
        // positive is specific monitor, no idea how it's decided. 1 isn't always primary
        if(true)fullScreen(P2D,-2);
        else
        {
            size(1800,900,P2D);
        }
        smooth(8);
        //surface.setIcon(icon);
        PJOGL.setIcon("data/BlueLogo2(007fff).png");
    }

    public void  setup(){
        System.out.println(
                        "    ______     ________            _          \n" +
                        "   / ____/__  / / ____/___  ____ _(_)___  ___ \n" +
                        "  / __/ / _ \\/ / __/ / __ \\/ __ `/ / __ \\/ _ \\\n" +
                        " / /___/  __/ / /___/ / / / /_/ / / / / /  __/\n" +
                        "/_____/\\___/_/_____/_/ /_/\\__, /_/_/ /_/\\___/ \n" +
                        "© 2017 Benjamin Welsh    /____/     \n");
        System.out.println("Dialog Editor v0.1\n");
        System.out.println("Initializing...");
        surface.setTitle("EelGame Dialog Editor");
        PFont font=createFont("data/segoeui.ttf",36);
        textFont(font);
        surface.setResizable(true);
        viewController=new ViewController(this);
        System.out.println("Initialized");
    }

    public void draw(){
        background(20,20,70);
        viewController.updatePan(mouseX-pmouseX,mouseY-pmouseY);
        viewController.drawGrid(false);
        // Center screen
        pushMatrix();
        translate(width/2,height/2);

        // Editor View
        pushMatrix();
        viewController.applyTransform(this);


        popMatrix();
        popMatrix();
    }

    public void mousePressed(){
        if(mouseButton==RIGHT)viewController.setViewGrabbed(true);
    }
    public void mouseReleased(){
        if(mouseButton==RIGHT)viewController.setViewGrabbed(false);
    }
    public void mouseMoved(){

        //if(mouseButton==RIGHT)gc.model.hand.kill(gc.model.selected);
    }
    public void mouseWheel(MouseEvent event) {
        int e = event.getCount();
        viewController.adjustZoom(e);
    }
    public void keyPressed(){
//        println(keyCode,key==CODED);
        //Movement

        if(keyCode==100){
        }else if(keyCode==99){
        }else if(keyCode==98){
        }
        //F5
        else if(keyCode==101){
        }else if(keyCode==102){
        }else if(keyCode==103){
        }else if(keyCode==104){
        }
        //F9
        else if(keyCode==105){
        }else if(keyCode==107){
        }else if(keyCode==106){
        }
//        else if (key == '[') {
//            gc.model.swapPlayers();
//        }else if (key == ']') {
//            gc.model.theirField.add(gc.model.selected);
//        }
//        if (key == 27){
//            key=0;
//            actionEscape();
//        }

    }
    public void keyReleased(){
    }
//    private void promptKeyInput(){
//        if(keyCode==BACKSPACE){
//            if(inputBuffer.length()>0)inputBuffer=inputBuffer.substring(0,inputBuffer.length()-1);
//        }else if(key=='\t'&&promptQuery==null){
//            promptMode=false;
//        }else if(key!=CODED&&(key+"").matches("[^|\\n\\r]")){
//            inputBuffer=inputBuffer+key;
//        }else if(key=='\n'){
//            promptMode=false;
//            handlePrompt();
//        }
//    }

    public static void main(String... args){
        PApplet.main("DialogEditor");
    }
}
