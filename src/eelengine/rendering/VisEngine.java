package eelengine.rendering;

import eelengine.Util;
import eelengine.Point;
import processing.core.PApplet;

import java.awt.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.TreeSet;

/**
 * Created by @BWelsh on 10/12/2017.
 * Stores an environment of barriers between points.
 * Can generate a list of points that form a visibility polygon
 * Can also render said polygon to a pgraphics
 *
 */
public class VisEngine {
    private ArrayList<Point> points = new ArrayList<>();
    private ArrayList<Integer> indA = new ArrayList<>();
    private ArrayList<Integer> indB = new ArrayList<>();

    public ArrayList<Point> getPoints() {
        return points;
    }

    public ArrayList<Integer> getIndA() {
        return indA;
    }

    public ArrayList<Integer> getIndB() {
        return indB;
    }

    private int getPointIndex(Point point){
        for(int i=0;i<points.size();i++)if(points.get(i).equals(point))return i;
        return -1;
    }
    public void addLine(Point a, Point b){
        int a_idx=getPointIndex(a);
        int b_idx=getPointIndex(b);
        // TODO check if edge already exists
        if(a_idx==-1){points.add(a);a_idx=points.size()-1;}
        if(b_idx==-1){points.add(b);b_idx=points.size()-1;}
        indA.add(a_idx);
        indB.add(b_idx);
    }
    public void drawCasts(PApplet p, int lx, int ly, float x1, float y1, float x2, float y2){
        ArrayList<Point> poly=new ArrayList<>();
        Point loc=new Point(lx,ly);
        Util.BTester bTester=new Util.BTester(x1,y1,x2,y2);
        if(!Util.in(lx,x1,x2)||!Util.in(ly,y1,y2))return;
        p.stroke(0,0,255);
        p.noFill();
        p.rect(x1,y1,x2-x1,y2-y1);
        TreeSet<Point> radar=new TreeSet<>((Point o1, Point o2)->
                (Math.atan2(o1.x - lx, o1.y - ly)>= Math.atan2(o2.x - lx, o2.y - ly))?1:-1);
        // hoo hoo hoo this boi think he so clever yes he do
        for(Point point:points){
            if(!bTester.test(point))continue;
            radar.add(point);
        }
        radar.add(new Point(x1,y1));
        radar.add(new Point(x2,y1));
        radar.add(new Point(x1,y2));
        radar.add(new Point(x2,y2));

        p.stroke(0,255,0);
        p.fill(0,255,0);
        int i=0;
        float maxlength=(float)Util.dist(x1,y1,x2,y2);
        Point pbefore,pafter,hit;
        for(Point point:radar){

            p.stroke(0,255,0);
            // DO the raytraces
            float isect=cast(loc,point,bTester);
            if(isect+0.01>=1) {
                // Now we need to do the offset traces

                hit=Util.intersectPointFromAmount(loc,point,isect);
                pbefore=Util.lineInDirection(loc,maxlength,Util.direction(loc,point)-0.0001);
                pafter=Util.lineInDirection(loc,maxlength,Util.direction(loc,point)+0.0001);
                float before=cast(loc,pbefore,bTester);
                float after=cast(loc,pafter,bTester);

                // NOTE it is possible that there's a worthwhile performance gain from
                // checking that the pre/post points aren't near the regular points
                poly.add(Util.intersectPointFromAmount(loc,pbefore,before));
                poly.add(hit);
                poly.add(Util.intersectPointFromAmount(loc,pafter,after));

            }
                //else System.err.println("(eelengine.rendering.VisEngine.drawCasts()) WHY THO");
            p.stroke(255,255,0);
            p.ellipse((float)point.x,(float)point.y,2,2);

        }

        // render product
        for(Point point:poly){
            p.line(lx,ly,(float)point.x,(float)point.y);
            p.text(i++,(float)point.x,(float)point.y);
        }
        p.noStroke();
        p.fill(255,70);
        p.beginShape();
        for(Point point:poly) {
            p.vertex((float) point.x, (float) point.y);
        }
        p.endShape();

    }

    private Point getA(int i, Util.BTester bTester) throws IndexOutOfBoundsException{
        switch (i){
            case -1:
            case -2: return new Point(bTester.x1,bTester.y1);
            case -3:
            case -4: return new Point(bTester.x2,bTester.y2);
            default: return points.get(indA.get(i));
        }
    }
    private Point getB(int i, Util.BTester bTester) throws IndexOutOfBoundsException{
        switch (i){
            case -1:
            case -3: return new Point(bTester.x1,bTester.y2);
            case -2:
            case -4: return new Point(bTester.x2,bTester.y1);
            default: return points.get(indB.get(i));
        }
    }
    /**
     * Returns the distance as a ratio of the line ab to the nearest wall within bounds. Returns 0 on no success.
     */
    private float cast(Point a, Point b, Util.BTester bTester){
        Point c,d;
        float shortest=0;
        for(int i=-4;i<indA.size();i++){
            c=getA(i,bTester);
            d=getB(i,bTester);
            if(bTester.test(c)||bTester.test(d)){
                if(Util.intersectInclusive(a,b,c,d)) {
                    float amt = (float) Util.intersectAmount(a, b, c, d);
                    if (amt!=Float.NaN&&amt > 0) {
                        if (shortest == 0) shortest = amt;
                        else shortest = Util.min(shortest, amt);
                    }
                }
            }
        }
        return shortest;
    }
}
