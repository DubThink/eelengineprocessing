package eelengine.rendering;

import eelengine.Trans;
import eelengine.Util;
import org.dyn4j.dynamics.Body;
import org.dyn4j.dynamics.BodyFixture;
import org.dyn4j.geometry.Circle;
import org.dyn4j.geometry.Polygon;
import org.dyn4j.geometry.Vector2;
import processing.core.PApplet;
import processing.core.PGraphics;
import sun.nio.cs.UTF_32LE;

import java.awt.*;

/**
 * Created by Benjamin on 6/15/2017.
 * Mostly render helper functions
 */
public class Renderer {
    public static void debugdraw(PGraphics p, Body body) {
        debugdraw(p,body,new Color(0,0,127,80),new Color(255,255,0));
    }
    public static void debugdraw(PGraphics p, Body body, Color stroke) {
        debugdraw(p,body,new Color(0,0,127,80),stroke);
    }
    public static void debugdraw(PGraphics p, Body body, Color fill, Color stroke){
        p.strokeWeight(1);
        p.stroke(Util.pColor(stroke));
        p.fill(Util.pColor(fill));
        p.pushMatrix();
        p.translate(Trans.x(body.getTransform().getTranslationX()),Trans.y(body.getTransform().getTranslationY()));
        p.rotate(Trans.r(body.getTransform().getRotation()));
        for(BodyFixture fixture:body.getFixtures()){
            if(fixture.getShape() instanceof Polygon) {
                Polygon convex = (Polygon) fixture.getShape();
                Vector2[] verts = convex.getVertices();
                p.beginShape();
                for (Vector2 vert : verts) {
                    p.vertex(Trans.x(vert.x), Trans.y(vert.y));
                }
                p.vertex(Trans.x(verts[0].x), Trans.y(verts[0].y));
                p.endShape();
                //Circle
            }else if(fixture.getShape() instanceof Circle){
                Circle convex=(Circle) fixture.getShape();
                float r=(float)convex.getRadius();
                p.ellipse(0,0,Trans.n(r*2),Trans.n(r*2));
                p.line(0,0,Trans.x(r),0);
            }
        }
        p.popMatrix();
    }
}
