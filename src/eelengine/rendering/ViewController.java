package eelengine.rendering;

import eelengine.Trans;
import eelengine.Util;
import processing.core.PApplet;

/**
 * Created by Benjamin on 6/14/2017.
 */
public class ViewController {
    PApplet p;
    public float viewDisplacementX=0;
    public float viewDisplacementY=0;
    private boolean viewGrabbed=false;
    public int zoomLevel=0;
    private int ZOOM_MAX=3;
    private int ZOOM_MIN=-10;
    private double ZOOM_COEFFICIENT=Math.pow(1.999,1/3.0);

    public ViewController(PApplet p) {
        this.p = p;
    }

    public void updatePan(float dX, float dY){
        if(viewGrabbed){
            viewDisplacementX+=dX*getInvZoomFactor();
            viewDisplacementY+=dY*getInvZoomFactor();
        }
    }

    public void applyTransform(PApplet p){
        p.scale(getZoomFactor());
        p.translate(viewDisplacementX,viewDisplacementY);
    }

    public float getZoomFactor(){
        return (float)Math.pow(ZOOM_COEFFICIENT,zoomLevel);
    }
    public float getInvZoomFactor(){
        return 1/getZoomFactor();
    }

    public boolean isViewGrabbed() {
        return viewGrabbed;
    }

    public void setViewGrabbed(boolean viewGrabbed) {
        this.viewGrabbed = viewGrabbed;
    }

    public float toWorldX(float screenX){
        float z=1/getZoomFactor();
        return ( (screenX-(p.width/2))*z/ Trans.scale)-viewDisplacementX/Trans.scale;
    }
    public float toWorldY(float screenY){
        float z=1/getZoomFactor();
        return (( (-screenY+(p.height/2))*z/Trans.scale)+viewDisplacementY/Trans.scale);
    }
    public double toScreenX(double worldX){
        return ((worldX* Trans.scale+viewDisplacementX)*getZoomFactor())+(p.width/2);
    }

    public double toScreenY(double worldY){
        return -((worldY*Trans.scale-viewDisplacementY)*getZoomFactor())+(p.height/2);
    }

    public void adjustZoom(int delta){
        zoomLevel= Util.clamp(zoomLevel-delta,ZOOM_MIN,ZOOM_MAX);
    }

    public void drawGrid(boolean quarters){
        float num=(quarters&&getZoomFactor()>0.35)?.25f:1.0f;
        int startX=(int)Math.floor(toWorldX(0));
        int endX=(int)Math.ceil(toWorldX(p.width));
        int endY=(int)Math.ceil(toWorldY(0));
        int startY=(int)Math.ceil(toWorldY(p.height))-1;
//        p.println(startY,endY,toScreenY(-15));
        for(float i=startX;i<=endX;i+=num){
            if(i==0)p.stroke(0,255,0,127);
            else if(i%16==0)p.stroke(255,127);
            else if(i%4==0)p.stroke(170,127);
            else if(i%1==0)p.stroke(90,127);
            else p.stroke(35,127);
            p.line((float)toScreenX(i),0,(float)toScreenX(i),p.height);
        }
        for(float i=startY;i<=endY;i+=num){
            if(i==0)p.stroke(255,0,0,127);
            else if(i%16==0)p.stroke(255,127);
            else if(i%4==0)p.stroke(170,127);
            else if(i%1==0)p.stroke(90,127);
            else p.stroke(35,127);
            p.line(0,(float)toScreenY(i),p.width,(float)toScreenY(i));
        }
    }
}
