package eelengine;

import eelengine.physics.CollisionCallback;
import eelengine.physics.ProjectilePawn;
import org.dyn4j.dynamics.Body;
import org.dyn4j.geometry.Vector2;
import processing.core.PApplet;

/**
 * Created by Benjamin on 6/15/2017.
 * Game logic class for projectiles
 * Uses ProjectilePawn
 */
public abstract class Projectile extends GameObject implements CollisionCallback {
    private Projectile(PApplet p, ProjectilePawn pawn) {
        super(pawn);
        mainSprite=new Sprite(p,"data/Fireball1.png","data/Fireball2.png","data/Fireball3.png","data/Fireball4.png");
        mainSprite.startAnimation(80);
    }
    public Projectile(PApplet p, float x, float y, Vector2 velocity, int team){
        this(p,new ProjectilePawn(x,y,velocity,team));
    }
    public Projectile(PApplet p, Vector2 position, Vector2 velocity, int team){
        // TODO fix all this mixed vectors stuff
        this(p,new ProjectilePawn((float) position.x,(float)position.y,velocity,team));
    }
    @Override
    public void onCollision(Body other) {
        //System.out.println("Bullet Collided");
        if(other.getUserData() instanceof GameObject)onProjectileHit((GameObject)other.getUserData());
    }

    protected abstract void onProjectileHit(GameObject gameObject);
}
