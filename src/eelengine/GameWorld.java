package eelengine;

import eelengine.event.EventQueue;
import eelengine.navigation.Navigation;
import eelengine.physics.CollisionCallback;
import eelengine.physics.ContactCallback;
import eelengine.physics.Pawn;
import org.dyn4j.collision.AxisAlignedBounds;
import org.dyn4j.dynamics.*;
import org.dyn4j.dynamics.contact.ContactAdapter;
import org.dyn4j.dynamics.contact.ContactPoint;
import org.dyn4j.geometry.Vector2;
import processing.core.PApplet;
import processing.core.PGraphics;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Benjamin on 5/27/2017.
 * The main class to handle objects contained within the current level
 * Handles object cleanup, setting up physics including callbacks, navigation loading
 */
public class GameWorld {
    public static String mapDataDir="data/maps/";
    public Navigation navigation;
    protected EventQueue physicsEventQueue=new EventQueue();
    public World world;
    protected int width,height;
    protected String mapName;
    ArrayList<Wall> walls=new ArrayList<>();

    ArrayList<GameObject> gameObjects = new ArrayList<>();
    public GameWorld(int width, int height, String mapName) {
        this.width=width;
        this.height=height;
        this.mapName=mapName;
        world =new World(new AxisAlignedBounds(width,height));
        world.setGravity(new Vector2(0,0));

        // Initialize Physics event handlers
        world.addListener(new CollisionAdapter(){
            @Override
            public boolean collision(Body body1, BodyFixture fixture1, Body body2, BodyFixture fixture2) {
                //System.out.println("Collision between"+body1.getId()+" and "+body2.getId());
                if(body1.getUserData() instanceof CollisionCallback){
                    physicsEventQueue.addEvent(new CollisionCallback.CollisionEvent((CollisionCallback) body1.getUserData(),body2));
                }
                if(body2.getUserData() instanceof CollisionCallback){
                    physicsEventQueue.addEvent(new CollisionCallback.CollisionEvent((CollisionCallback) body2.getUserData(),body1));
                }
                return super.collision(body1, fixture1, body2, fixture2);
            }
        });

        world.addListener(new ContactAdapter(){
            @Override
            public boolean begin(ContactPoint point) {
                if(point.getBody1().getUserData() instanceof ContactCallback){
                    physicsEventQueue.addEvent(new ContactCallback.ContactEvent((ContactCallback) point.getBody1().getUserData(),point.getBody2(),point));
                }
                if(point.getBody2().getUserData() instanceof ContactCallback){
                    physicsEventQueue.addEvent(new ContactCallback.ContactEvent((ContactCallback) point.getBody2().getUserData(),point.getBody1(),point));
                }
                return super.begin(point);
            }
        });
        world.addListener(new StepAdapter(){
            @Override
            public void end(Step step, World world) {
                physicsEventQueue.fireEvents();
                super.end(step, world);
            }
        });
        reloadNav();
    }

    public void addWall(float x1,float y1,float x2, float y2,boolean halfWall){
        walls.add(new Wall(Math.abs(x2-x1),Math.abs(y2-y1),(float) Util.halfBetween(x1,x2),(float) Util.halfBetween(y1,y2),world,halfWall));
    }
    public void drawWalls(PApplet p){
        for(Wall wall:walls){
            wall.debugDraw(p);
        }
    }
    public void drawNav(PApplet p){
//        System.out.println("Draw nav happened");
        navigation.drawNodes(p);
    }
    public void reloadNav(){
        navigation=new Navigation(-100,-100,100,100,mapDataDir+mapName+".nav");
    }
    public void saveNav(){
        navigation.saveNodes(mapDataDir+mapName+".nav");
    }
    private Wall[] getWallsAtLocation(float x, float y){
        return null;
        //TODO do this
    }

    public void physicsStep(double dt){
        world.update(dt);
    }
    public void renderStep(PGraphics p){
        EngineProfilerTool.stampIn("World render");
        gameObjects.forEach(gameObject -> {
            gameObject.draw(p);
        });
        EngineProfilerTool.stampOut("World render");
    }

    public void spawnObject(GameObject object){
        if(object.getPawn()!=null)object.getPawn().spawnInWorld(world);
        gameObjects.add(object);
    }

    /**
     * Removes all gameobject flagged with kill.
     */
//    protected void cleanObjects(){
//        for(GameObject gameObject: gameObjects)if(gameObject.kill)gameObjects.kill(gameObject);
//    }

    /**
     * Removes all gameobject flagged with kill.
     */
    public void cleanObjects(){
//        for(int i=gameObjects.size()-1;i>0;i--){
//            if(gameObjects.get(i).kill);//gameObjects.kill(i);
//        }
        EngineProfilerTool.stampIn("world cleaner");
        for(int i=gameObjects.size()-1;i>=0;i--){
            if(gameObjects.get(i).kill){
                gameObjects.get(i).getPawn().removeFromWorld(world);
                gameObjects.remove(i);
            }
        }
//        Iterator<GameObject> iterator = gameObjects.iterator();
//        GameObject ref;
//        while(iterator.hasNext()){
//            ref=iterator.next();
//            if(ref.kill)iterator.kill();
//        }
        EngineProfilerTool.stampOut("world cleaner");

    }
    public static void main(String[] args) {
        GameWorld gameWorld1 =new GameWorld(100,100,"gameWorld2");
        GameWorld gameWorld2 =new GameWorld(100,100,"gameWorld2");
        long start=System.nanoTime();
        System.out.println(System.nanoTime()-start);
        for(int i=0;i<10000;i++) {
            gameWorld1.spawnObject(new GameObject(new Pawn(0, 0)) {
            });
            gameWorld2.spawnObject(new GameObject(new Pawn(0, 0)) {
            });
        }
        System.out.println(System.nanoTime()-start);

        Random random=new Random();
        for(int i=0;i<50;i++){
            int a=random.nextInt(10000);
            gameWorld1.gameObjects.get(a).kill();
            gameWorld2.gameObjects.get(a).kill();
        }
//        System.out.println(System.nanoTime()-start+" Trial 1 start");
//        gameWorld1.cleanObjects();
//        System.out.println(System.nanoTime()-start+" Trial 1 end");
        long time1,time2,time3;
        time1=System.nanoTime()-start;
        time2=System.nanoTime()-start;
        gameWorld2.cleanObjects();
        time3=System.nanoTime()-start;
        System.out.println(time1+" Trial 2 pre");
        System.out.println(time2+" Trial 2 start");
        System.out.println(time3+" Trial 2 end");
        System.out.println((time3-time2)/1000000.0+" Trial 2 elapsed ms");


    }
}
