package eelengine;

import eelengine.physics.Physics;
import eelengine.rendering.Renderer;
import org.dyn4j.collision.CategoryFilter;
import org.dyn4j.dynamics.Body;
import org.dyn4j.dynamics.BodyFixture;
import org.dyn4j.dynamics.World;
import org.dyn4j.geometry.MassType;
import org.dyn4j.geometry.Rectangle;
import processing.core.PApplet;

import java.awt.*;


/**
 * Created by Benjamin on 5/27/2017.
 */
public class Wall {
    float w,h;
    boolean halfwall;
    Body collision;
    public Wall(float w, float h, float x, float y, World world) {
        this(w, h, x, y, world, false);
    }
    public Wall(float w, float h, float x, float y, World world,boolean halfWall) {
        this.w = w;
        this.h = h;
        this.halfwall=halfWall;
        Rectangle rect = new Rectangle(w,h);
        collision=new Body();
        BodyFixture wall=new BodyFixture(rect);
        wall.setFilter(new CategoryFilter(halfWall? Physics.HALFWALL:Physics.WALL,Physics.M_ALL));
        //if(halfWall)wall.setSensor(true);
        collision.addFixture(wall);
        collision.setMass(MassType.INFINITE);
        collision.translate(x,y);
        world.addBody(collision);

    }
    public void debugDraw(PApplet p){
        if(halfwall) Renderer.debugdraw(p.g,collision, new Color(255,127,0));
        else Renderer.debugdraw(p.g,collision, new Color(255,255,0));

    }
}
