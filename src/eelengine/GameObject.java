package eelengine;

import eelengine.physics.Pawn;
import processing.core.PConstants;
import processing.core.PGraphics;

/**
 * Created by Benjamin on 6/15/2017.
 * Basic in-game object that has a pawn
 */
public abstract class GameObject {
    protected Pawn pawn;
    protected Sprite mainSprite;
    protected boolean kill =false;
    protected double rot=0;

    //GameWorld ownerGameWorld =null;

    public GameObject(Pawn pawn) {
        this.pawn = pawn;
        System.out.println("PAWN STATE: (GameObject)"+(this.pawn==null)+"  "+(pawn==null));
        pawn.setParent(this);
    }

    /** The primary function in which to implement game logic
     *
     */
    public void update(){}

    public float getX(){
        return Trans.x(pawn.getX());
    }

    public float getY(){
        return Trans.y(pawn.getY());
    }

    public float getRot(){
        return Trans.r(rot);
    }

    public void draw(PGraphics p){
        updateRotation();
        p.pushMatrix();
        p.translate(getX(),getY());
        p.rotate(getRot());
        drawSelf(p);
        p.popMatrix();
    }
    private void updateRotation(){
        if(pawn.getVelocity()>0.05){
            rot=pawn.getForwardRotation();
        }
    }
    /**
     * Override this
     */
    private void drawSelf(PGraphics p){
        p.stroke(0,0,255);
        p.fill(100,0,0,80);
        p.imageMode(PConstants.CENTER);
        p.image(mainSprite.current(),0,0);
        p.imageMode(PConstants.CORNER);
    }
    public Pawn getPawn(){
        return pawn;
    }
    public void kill(){
        kill=true;
    }
}
