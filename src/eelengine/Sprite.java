package eelengine;

import processing.core.PApplet;
import processing.core.PImage;

import java.util.ArrayList;

/**
 * Created by Benjamin on 5/26/2017.
 * A basic sprite class for EelGame
 * Supports animation or multiple skins
 */
public class Sprite {
    private ArrayList<PImage> images=new ArrayList<>();
    private boolean animate=false;
    //private boolean hide=false;
    private double msPerFrame;
    private long animStartTime;
    private int skin=0;
    public Sprite(PApplet p, String ... names){
        for(String name:names)images.add(p.loadImage(name));
    }
    public void startAnimation(double msPerFrame){
        this.msPerFrame=msPerFrame;
        animStartTime=System.currentTimeMillis();
        animate=true;
    }
    public void stopAnimation(){
        animate=false;
        skin=getCurrentAnimatedSkinNum();
    }
    public int skinCount(){
        return images.size();
    }
    public void setSkin(int num){
        skin=num;
    }
    public PImage current(){
        //System.out.println(getCurrentAnimatedSkinNum()+" "+((System.currentTimeMillis()-animStartTime)/msPerFrame));
        if(animate){
            return images.get(getCurrentAnimatedSkinNum());
        }else return images.get(skin%images.size());
    }
    private int getCurrentAnimatedSkinNum(){
        return (int)Math.floor((System.currentTimeMillis()-animStartTime)/msPerFrame)%images.size();
    }
}
