package eelengine;

import eelengine.physics.DriverPawn;
import processing.core.PApplet;

/**
 * Created by Benjamin on 5/26/2017.
 */
public class Character extends GameObject {
//    protected Sprite s_body;
//    protected DriverPawn pawn;
    public Character(PApplet p, DriverPawn pawn) {
        super(pawn);
        System.out.println("PAWN STATE: (Character)"+(this.pawn==null)+"  "+(pawn==null));
        this.pawn=pawn;  // Needed because pawn upgraded to DriverPawn
        mainSprite=new Sprite(p,"data/character/bambito.png");
    }

    private void updateRotation(){
        if(pawn.getVelocity()>0.05){
            rot=pawn.getForwardRotation();
        }
    }

    @Override
    public DriverPawn getPawn() {
        return (DriverPawn) pawn;
    }
    //    /**
//     * Override this
//     */
//    private void drawSelf(PGraphics p){
//        p.stroke(0,0,255);
//        p.fill(100,0,0,80);
//        p.imageMode(PConstants.CENTER);
//        p.image(s_body.current(),0,0);
//        p.imageMode(PConstants.CORNER);
//    }
}
