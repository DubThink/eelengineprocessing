package eelengine;

/**
 * Created by Benjamin on 5/25/2017.
 * Provides helper sclaing functions
 */
public class Trans {
    public static float scale = 100;
    public static float x(double x){
        return (float)x*scale;
    }
    public static float ix(double x){
        return (float)x/scale;
    }
    public static float y(double y){
        return -(float)y*scale;
    }
    public static float iy(double y){
        return -(float)y/scale;
    }
    public static float r(double r){
        return -(float)r;
    }
    public static float n(double n){
        return (float)n*scale;
    }
}
