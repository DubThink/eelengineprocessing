package eelengine;

import processing.core.PConstants;
import processing.core.PGraphics;

import java.util.*;

import static java.lang.Float.min;

/**
 * Created by Benjamin on 6/17/2017.
 * Used for monitoring performance of engine components
 * call stampIn(IDENTIFIER) to start the timer on engine part IDENTIFIER
 * call stampOut(IDENTIFIER) to end the timer on engine part IDENTIFIER
 * call reset() at the end of an engine tick to clear and stop all timers
 * Note: timers are parallel: any timer can be toggled on and off at any point
 */
public class EngineProfilerTool {
    /**
     * Display data container
     */
    static class TimeDisplay{
        long micros;
        float percentMax;
        String name;

        public TimeDisplay(long micros, float percentMax, String name) {
            this.micros = micros;
            this.percentMax = percentMax;
            this.name = name;
        }
    }

    static HashMap<String, TimeSample> db = new HashMap<>();

    /**
     * Stamps in a timer with id id. If the timer is already stamped in, does nothing.
     * @param id The id to stamp in
     */
    public static void stampIn(String id){
        if(!db.containsKey(id))db.put(id,new TimeSample(id));
        TimeSample sample = db.get(id);
        sample.start(System.nanoTime()/1000);
    }

    /**
     * Stamps out a timer with id id.
     * @param id The id to stamp in
     */
    public static void stampOut(String id){
        if(!db.containsKey(id))db.put(id,new TimeSample(id));
        TimeSample sample = db.get(id);
        sample.end(System.nanoTime()/1000);
    }

    /**
     * stops all timers, updates the moving averages, and zeros out all timers
     * call at the end of a cycle
     */
    public static void reset(){
        for(TimeSample sample:db.values()){
            sample.end(System.nanoTime()/1000);
            sample.next();
        }
    }

    public static TimeDisplay[] getDisplay(boolean avgMode){
        if(db.size()==0)return null;
        ArrayList<TimeSample> samples=new ArrayList<>(db.values());
        Collections.sort(samples,comparesDelta);
        TimeDisplay output[]=new TimeDisplay[samples.size()];
        long maxDelta=avgMode? (long)(samples.get(samples.size()-1).floatingAvg*0.1):samples.get(samples.size()-1).lastDelta;
//        System.out.println("MaxDelta: "+maxDelta);
        for(int i=0;i<samples.size();i++) {
//            System.out.println(samples.get(i));
            if(avgMode)output[i]=new TimeDisplay((long)(samples.get(i).floatingAvg*0.1),(float)(samples.get(i).floatingAvg*0.1/maxDelta),samples.get(i).name);
            else             output[i]=new TimeDisplay(samples.get(i).lastDelta,samples.get(i).lastDelta/maxDelta,samples.get(i).name);

        }
        return output;
    }
    static Comparator<TimeSample> comparesDelta = (o1, o2) -> o1.lastDelta<o2.lastDelta?-1:o1.lastDelta>o2.lastDelta?1:0;

    /**
     * renders a meter at location x,y on PGraphics p
     * @param p
     * @param x
     * @param y
     */
    public static void drawMeter(PGraphics p, int x, int y){
        p.pushMatrix();
        p.translate(x,y);
        p.textSize(12);
        TimeDisplay[] times=getDisplay(true);
        for(int i=0;i<times.length;i++){
            p.noStroke();
            p.rectMode(PConstants.CORNERS);
            p.colorMode(PConstants.HSB);
            p.fill(150-min(times[i].micros/30,150),255,200,127);
            p.rect(4,i*12-1,4+times[i].micros/10,i*12-1+10);
            p.colorMode(PConstants.RGB);
            p.fill(255);
            p.text(times[i].name+": "+times[i].micros/1000.0+"ms",4,i*12+10);
        }
        p.popMatrix();
    }
}

class TimeSample{
    String name;

    public TimeSample(String name) {
        this.name = name;
    }
    double floatingAvg=0;
    long lastDelta=0;
    private long startTime=0;
    private boolean active=false;
    void start(long time){
        if(!active)startTime=time;
        active=true;
    }
    void end(long time){
        lastDelta+=time-startTime;
        active=false;
    }
    void next(){
        floatingAvg=(floatingAvg+lastDelta)*0.9;
        lastDelta=0;
        active=false;
    }
    @Override
    public String toString(){
        return name+": Avg="+floatingAvg/10000.0+"ms last="+lastDelta/1000.0+"ms ";
    }
}
