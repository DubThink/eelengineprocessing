package eelengine;

import eelengine.physics.DriverPawn;
import eelengine.physics.Pawn;
import org.dyn4j.geometry.Vector2;
import processing.core.PApplet;
import eelengine.navigation.*;
import processing.core.PGraphics;

/**
 * Created by Benjamin on 5/26/2017.
 */
public class AICharacter extends Character implements Damageable{
    //public static boolean debugDrawAI=true;
    //private boolean isTargetPawn =false;
    //private boolean targeting=false;
    private Pawn targetPawn;
    private Navigation navigation;
    NavPath path;
    private int health=10;
    public AICharacter(PApplet p, Navigation navigation, DriverPawn pawn) {
        super(p, pawn);
        System.out.println("PAWN STATE: (AIC)"+(this.pawn==null)+"  "+(pawn==null));
        //pawn.setCollisionFilter(Physics.MOB,Physics.M_WALLS);
        this.navigation=navigation;
    }
    public void setTargetPawn(Pawn target){
        targetPawn=target;
    }

    @Override
    public void draw(PGraphics p) {
        super.draw(p);
        if(path!=null)path.draw(p);
    }

    public void AITick(){
        if(targetPawn==null){
            rot+=0.05;//spin when not targeting
            return;
        }
        if(path==null)updatePath();
        if (path == null) {
            System.err.println("Unable to generate new path");
            return;
        }
        //Get direction to move
        Vector2 targetMoveDirection=path.getNextTargetVector(pawn.getX(), pawn.getY());
        if(targetMoveDirection==null)updatePath();
        if (path == null) {
            System.err.println("Unable to generate new path");
            return;
        }else {
            targetMoveDirection=path.getNextTargetVector(pawn.getX(), pawn.getY());
        }
        targetMoveDirection.normalize();
        getPawn().setMoveVector(targetMoveDirection);


    }
    private void updatePath(){
        //regen path
        path = navigation.findPath(pawn.getX(), pawn.getY(), targetPawn.getX(), targetPawn.getY());
        if(path!=null)path.setTargetPawn(targetPawn);
    }
    private void searchForTarget(){

    }

    @Override
    public void addDamage(int amount, int damageType) {
        health-=amount;
        System.out.println("AI took "+amount+" damage. Health: "+health);
    }
}
