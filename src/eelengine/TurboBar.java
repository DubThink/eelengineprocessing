package eelengine;

import processing.core.PApplet;
import processing.core.PImage;

import java.awt.*;
import java.util.ArrayList;

/**
 * Created by benja on 10/4/2017.
 */
public class TurboBar {
    float value;
    float maxVal=1;
    Color backgroundColor=new Color(40,40,40);
    Color barColor=Color.red;
    //,fillColor,emptyColor;
    ArrayList<Bit> bits=new ArrayList<>();
    public static PImage ebar;
    class Bit {
        public Bit(float pos, float size, long time, boolean out) {
            this.pos = pos;
            this.size = size;
            this.out = out;
            this.time=time;
        }

        float pos;
        float size;
        long time;
        boolean out;
    }

    public TurboBar(float value) {
        this.value = value;
    }
    public static float fadeTime=150;
    public void render(PApplet p, int x, int y, int w, int h){
        //System.out.println(value);
        p.noStroke();
        p.fill(Util.pColor(backgroundColor));
        p.rect(x,y,w,h);
        p.fill(Util.pColor(barColor));
        p.rect(x,y,w*Util.clamp(value/maxVal,0.0f,1.0f),h);

        long mtime=System.currentTimeMillis();
        for(int i=bits.size()-1;i>=0;i--){
            if(mtime-bits.get(i).time>fadeTime){
                bits.remove(i);
            }
        }
        if(bits.size()>0) System.out.println("Bits #"+bits.size());
        // bits
        for(Bit bit:bits){
            long dtime=mtime-bit.time;
            float dfactor=dtime/fadeTime;
            if(bit.out) {
                p.fill(255,dfactor*255,dfactor*255);
                dfactor+=1;
                p.rect(x + w * Util.clamp(bit.pos / maxVal, 0.0f, 1.0f),
                        h / 2 + y - dfactor * h / 2, Util.min(w * bit.size,-1), dfactor * h);
            }else{
                p.fill(dfactor*255,0,0);
                p.rect(x + w * Util.clamp(bit.pos / maxVal, 0.0f, 1.0f),
                        y, 1 + w * bit.size, h-dfactor * h);
                p.rect(x + w * Util.clamp(bit.pos / maxVal, 0.0f, 1.0f),
                        y+h, 1 + w * bit.size, -h+dfactor * h);
                //h / 2 + y - dfactor * h / 2
            }

        }

    }
    public void setValue(float value){
        if(value!=this.value)
        bits.add(new Bit(this.value,value-this.value, System.currentTimeMillis(),value-this.value<0));
        this.value=value;
    }
}
