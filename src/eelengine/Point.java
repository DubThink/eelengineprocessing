package eelengine;

/**
 * Created by benja on 10/13/2017.
 * Pretty much java.util's point except with doubles
 */
public class Point {
    public double x;
    public double y;

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }
    public Point() {
        this(0, 0);
    }
    public Point(Point p) {
        this(p.x, p.y);
    }
    public boolean equals(Object obj) {
        if (obj instanceof Point) {
            Point pt = (Point)obj;
            return (x == pt.x) && (y == pt.y);
        }
        return super.equals(obj);
    }
    public String toString() {
        return getClass().getName() + "[x=" + x + ",y=" + y + "]";
    }
}
