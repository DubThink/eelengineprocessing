package eelengine.event;

/**
 * Created by Benjamin on 6/15/2017.
 */
public interface Event {
    public void fire();
}