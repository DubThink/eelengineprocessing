package eelengine.event;

import java.util.LinkedList;

/**
 * Created by Benjamin on 6/15/2017.
 */
public class EventQueue {
    LinkedList<Event> queue=new LinkedList<>();

    /**
     * Adds an event to the queue.
     * @param e
     */
    public void addEvent(Event e){
        //System.out.println(e.getClass());
        queue.add(e);
    }

    /**
     * Fires all events in the order they were added and removes them.
     */
    public void fireEvents(){
        while(queue.size()>0){
            queue.poll().fire();
        }
    }
}
