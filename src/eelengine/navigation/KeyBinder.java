package eelengine.navigation;

import java.util.ArrayList;

/**
 * Created by benja on 9/13/2017.
 */
public class KeyBinder {
    ArrayList<Binding> keyBinds=new ArrayList<>();

    public KeyBinder(Binding ... binds) {
        for(Binding b:binds)keyBinds.add(b);
    }

    public void add(Binding binding){
        keyBinds.add(binding);
    }

    public void keyDown(int keyID){
        keyBinds.parallelStream().forEach((binding) -> {
           if(keyID==binding.keyID)binding._down();
        });
    }

    public void keyUp(int keyID){
        keyBinds.parallelStream().forEach((binding) -> {
            if(keyID==binding.keyID)binding._up();
        });
    }


    public static class Binding{
        protected int keyID;
        private boolean isDown=false;
        public String name;
        public Binding(int keyID) {
            this.keyID = keyID;
        }

        public int getKeyID() {
            return keyID;
        }

        public boolean isDown() {
            return isDown;
        }
        private void _up(){isDown=false;up();}
        private void _down(){isDown=true;down();}
        public void down(){}
        public void up(){}
    }
}
