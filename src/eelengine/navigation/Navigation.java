package eelengine.navigation;

import eelengine.Trans;
import processing.core.PApplet;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.*;

/**
 * Created by Benjamin on 5/26/2017.
 */
public class Navigation {
    private int w,h,offsetX,offsetY;
    boolean[][] data;
    ArrayList<Node>nodes=new ArrayList<>();
    int nextID=0;
    public Navigation(int x1,int y1,int x2,int y2){
        w=x2-x1;
        h=y2-y1;
        offsetX=x1;
        offsetY=y1;
        data=new boolean[w][h];
    }
    public Navigation(int x1,int y1,int x2,int y2,String file){
        this(x1, y1, x2, y2);
        HashMap<Integer,Node> linker=new HashMap<>();
        // Load nodes
        try {
            {
                Scanner scanner = new Scanner(Paths.get(file));
                while (scanner.hasNextLine()) {
                    String line = scanner.nextLine();
                    if (line.startsWith("//")) continue; // Ignore comments
                    try {
                        Scanner lineScanner = new Scanner(line);
                        int id=lineScanner.nextInt();
                        int newid=nextID++;
                        Node node=new Node(newid,lineScanner.nextInt(),
                                lineScanner.nextInt(),
                                lineScanner.nextInt(),
                                lineScanner.nextInt());

                        linker.put(id,node);
                        nodes.add(node);

                    } catch (InputMismatchException e) {
                        System.err.println("Invalid nav line \"" + line + "\" (file \"" + file + "\")");
                    }

                }
                scanner.close();
            }
            {
                Scanner scanner = new Scanner(Paths.get(file));
                while (scanner.hasNextLine()) {
                    String line = scanner.nextLine();
                    if (line.startsWith("//")) continue; // Ignore comments
                    try {
                        Scanner lineScanner = new Scanner(line);
                        Node node = linker.get(lineScanner.nextInt());
                        lineScanner.nextInt();
                        lineScanner.nextInt();
                        lineScanner.nextInt();
                        lineScanner.nextInt();

                        while(lineScanner.hasNext()){
                            int id=lineScanner.nextInt();
                            node.addNeighbor(linker.get(id));
                        }
                    } catch (InputMismatchException e) {
                        System.err.println("Invalid nav line \"" + line + "\" (file \"" + file + "\")");
                    }

                }
                scanner.close();
            }
        }catch (IOException e){
            System.err.println("Unable to load nav file \""+file+"\"");
        }
    }
    public void drawNodes(PApplet p){
        for(Node node:nodes){
            node.draw(p);
        }
        for(Node node:nodes){
            node.drawLinks(p);
        }
    }
    public void printNodes(){
        for(Node node:nodes)
            System.out.println(node.toDataString());
    }
    public void saveNodes(String filename){
        try {
            FileWriter writer = new FileWriter(filename);
            writer.write("// EelGame Nav file\n");
            writer.write("// id x1 y1 x2 y2 nbr1 nbr2\n");
            for(Node node:nodes) {
                writer.write(node.toDataString()+"\n");
            }
            writer.close();
        }catch (IOException e){
            System.err.println("Unable to save nav data to file \""+filename+"\"");
        }
    }
    public void testFunction(){
        System.out.println(nodes.get(0).aabb(0,0,1,1));
    }
    /**
     * returns true if the specified region contains no nodes
     * @param x1
     * @param y1
     * @param x2
     * @param y2
     * @return
     */
    public boolean testRegion(int x1, int y1, int x2, int y2){
        for(Node node:nodes){
            if(node.aabb(x1, y1, x2, y2))return false;
        }
        return true;
    }
    public void makeNode(int x1, int y1, int x2, int y2){
        int newid=nextID++;
        nodes.add(new Node(newid,x1,y1,x2,y2));
    }

    /**
     * If the coords are integer values, it gets the node in the positive direction (by adding .5 to x and y)
     * @return
     */
    public Node getNodeAt(double x, double y){
        if(x%1==0)x+=.5;
        if(y%1==0)y+=.5;
        for(Node node:nodes){
            if(node.pointIn(x,y))return node;
        }
        return null;
    }
    public void removeNode(Node node){
        nodes.remove(node);
        for(Node node2:nodes) {
            node2.removeNeighbor(node);
        }
    }

    public void buildNeighbors(){
        for(int i=0;i<nodes.size()-1;i++){
            Node nodeA=nodes.get(i);
            for(int j=i+1;j<nodes.size();j++){
                Node nodeB=nodes.get(j);
                if(nodeA.aabbInclusive(nodeB)){
                    nodeA.addNeighbor(nodeB);
                    nodeB.addNeighbor(nodeA);
                }
            }
        }
    }
    // PATHFINDING
    public int maxNavSearchLength=1000;

    public NavPath findPath(double x1, double y1, double x2, double y2){
        resetNodes();
        boolean dbg=true;
        // STEP 1
        // find start and end nodes
        Node startNode=getNodeAt(x1,y1);
        Node endNode=getNodeAt(x2,y2);
        //TODO use getNodeNear so nav doesn't break on border cases
        if(startNode==null||endNode==null)return null;
        if(startNode==endNode){
            //TODO return simple nav
            if(dbg) System.out.println("Same eelengine.navigation.Node");
        }
        PriorityQueue<Tendril> queue=new PriorityQueue<>(10,new TendrilComparator());
        Tendril tendril=new Tendril(startNode,(float)x1,(float)y1);
        queue.add(tendril);
        startNode.insertTendril(tendril);
        for(int iterNum=0;iterNum<maxNavSearchLength;iterNum++){
            if(queue.size()==0){
                System.err.println("Iternum "+iterNum);
                return null;
            }
            Tendril current=queue.poll();
            for(Node neighbor:current.node.neighbors){
                Tendril next=new Tendril(current,neighbor);
                if(neighbor.insertTendril(next)){
                    queue.add(next);
                    if(dbg) System.out.println("Adding tendril to node "+next.node.id);
                }
            }
            if(endNode.tendril!=null){
                return new NavPath(this,endNode.tendril,x2,y2);
            }
        }
        return null;
    }
    private void resetNodes(){
        for(Node node:nodes)node.tendril=null;
    }

}


class Tendril{
    Tendril previous;
    Node node;
    //double weight;
    float x,y;
    boolean isEnd=false;
    public Tendril(Node node,float x, float y) {
        this.previous = null;
        this.node = node;
        this.x=x;
        this.y=y;
        isEnd=true;
        //weight=0;
    }
    public Tendril(Tendril previous, Node node) {
        this.previous = previous;
        this.node = node;
    }
    public void draw(PApplet p){
        if(previous==null){
            p.stroke(0,255,0);
            p.ellipse(Trans.x(getX()),Trans.y(getY()),20,20);
            return;
        }
//        p.beginShape();
//        p.stroke(255,0,0);
//        p.vertex(eelengine.Trans.x(previous.getX()),eelengine.Trans.y(previous.getY()));
//        p.stroke(0,255,0);
//        p.vertex(eelengine.Trans.x(node.centerX()),eelengine.Trans.y(node.centerY()));
//        p.endShape();
        p.stroke(0,255,0);
        p.line(Trans.x(previous.getX()),Trans.y(previous.getY()),Trans.x(node.centerX()),Trans.y(node.centerY()));
        previous.draw(p);
    }
    public double getX(){
        if(isEnd)return x;
        else return node.centerX();
    }
    public double getY(){
        if(isEnd)return y;
        else return node.centerY();
    }
    public double getLength(){
        if(isEnd)return 0;
        return node.distTo(previous.getX(),previous.getY());
    }
    public double getChainLength(){
        if(isEnd)return 0;
        else return previous.getChainLength()+getLength();
    }
    public void print(){
        System.out.println("eelengine.navigation.Tendril links to node "+node.id+" with length "+getLength()+". Total chain length: "+getChainLength());
        if(previous!=null)previous.print();
    }
    public void buildNodeChain(ArrayList<Node> chain){
        if(previous!=null)previous.buildNodeChain(chain);
        if(node!=null)chain.add(node);
    }
}
class TendrilComparator implements Comparator<Tendril>{
    @Override
    public int compare(Tendril o1, Tendril o2) {
        return Double.compare(o1.getChainLength(),o2.getChainLength());
    }
}
