package eelengine.navigation;

import eelengine.Trans;
import processing.core.PApplet;
import processing.core.PConstants;

import java.util.ArrayList;

import eelengine.*;
/**
 * Created by Benjamin on 6/15/2017.
 */
public class Node{
    ArrayList<Node> neighbors=new ArrayList<>();
    int x1,y1,x2,y2;
    int id;
    Tendril tendril=null;

    public Node(int id,int x1, int y1, int x2, int y2) {
        this.id = id;
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
    }
    public void draw(PApplet p){
        p.rectMode(PConstants.CORNERS);
        p.stroke(0,255,255);
        p.fill(255,70);
        p.rect(Trans.x(x1),Trans.y(y1),Trans.x(x2),Trans.y(y2));
        p.rectMode(PConstants.CORNER);
    }
    public void draw(PApplet p,int r, int g, int b){
        p.rectMode(PConstants.CORNERS);
        p.stroke(r,g,b);
        p.fill(255,70);
        p.rect(Trans.x(x1),Trans.y(y1),Trans.x(x2),Trans.y(y2));
        p.rectMode(PConstants.CORNER);
    }
    public void drawLinks(PApplet p){
        p.stroke(0,0,255);
        p.noFill();
        for(Node node:neighbors){
            p.bezier(Trans.x(centerX()),Trans.y(centerY()),
                    Trans.x(centerX()),Trans.y(centerY()+.75),
                    Trans.x(node.centerX()),Trans.y(node.centerY()-.75),
                    Trans.x(node.centerX()), Trans.y(node.centerY()));
        }
    }

    public double distTo(Node node) {
        return Util.dist(
                Util.halfBetween(x1, x2), Util.halfBetween(y1, y2),
                Util.halfBetween(node.x1, node.x2), Util.halfBetween(node.y1, node.y2)
        );
    }
    public double distTo(double x, double y){
        return Util.dist(Util.halfBetween(x1,x2), Util.halfBetween(y1,y2),x,y);
    }
    public double centerX(){
        return Util.halfBetween(x1,x2);
    }
    public double centerY(){
        return Util.halfBetween(y1,y2);
    }
    public String toDataString(){
        String me=id+" "+x1+" "+y1+" "+x2+" "+y2;
        String nids="";
        for(int i=0;i<neighbors.size();i++){
            nids+=" "+neighbors.get(i).id;
        }
        return me+nids;
    }
    public boolean aabb(Node node) {
        return Util.aabb(x1,y1,x2,y2,node.x1,node.y1,node.x2,node.y2);
    }
    public boolean aabbInclusive(Node node) {
        return Util.aabbInclusiveNoCorners(x1,y1,x2,y2,node.x1,node.y1,node.x2,node.y2);
    }
    public boolean aabb(int x1, int y1, int x2, int y2) {
        return Util.aabb(x1,y1,x2,y2,this.x1,this.y1,this.x2,this.y2);
    }
    public boolean pointIn(double x, double y){
        return Util.in(x,x1,x2)&& Util.in(y,y1,y2);
    }
    public void addNeighbor(Node node){
        if(!neighbors.contains(node)) neighbors.add(node);
    }
    public boolean removeNeighbor(Node node){
        return neighbors.remove(node);
    }
    public boolean insertTendril(Tendril newTendril){
        if(tendril==null){
            tendril=newTendril;
            return true;
        }else{
            if(tendril.getChainLength()>newTendril.getChainLength())tendril.previous=newTendril.previous;
            return false;
        }
    }
}
