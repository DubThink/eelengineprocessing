package eelengine.navigation;

import eelengine.physics.Pawn;
import eelengine.Trans;
import eelengine.Util;
import org.dyn4j.geometry.Vector2;
import processing.core.PGraphics;

import java.util.ArrayList;

/**
 * Created by Benjamin on 5/27/2017.
 */
public class NavPath {
    Tendril headTendril;
    Navigation navigation;
    Pawn targetPawn=null;
    double targetX;
    double targetY;
    ArrayList<Node> nodeChain;
    public NavPath(Navigation navigation,Tendril headTendril, double targetX, double targetY) {
        this.headTendril = headTendril;
        this.navigation = navigation;
        this.targetX = targetX;
        this.targetY = targetY;
        nodeChain=new ArrayList<>();
        headTendril.buildNodeChain(nodeChain);
    }

    public void setTargetPawn(Pawn targetPawn) {
        this.targetPawn = targetPawn;
    }

    public Vector2 getNextTargetVector(double x, double y){
        //Check path valid
        Node destinationNode;
        if(targetPawn!=null) {
            //System.out.println("setting destNode to node at targetPawn");
            destinationNode = navigation.getNodeAt(targetPawn.getX(), targetPawn.getY());
        }else {
            destinationNode = nodeChain.get(nodeChain.size()-1);
        }
        if(targetPawn!=null&&nodeChain.get(nodeChain.size()-1)!=destinationNode) {
            return null;
        }

        //Check if we're in the same cell
        Node currentNode=navigation.getNodeAt(x,y);
        if(destinationNode==currentNode){
            //System.out.println("Last cell");
            return Util.toPoint(x,y, getTargetX(), getTargetY());
        }
        //if we aren't, trim tree and target next cell
        if(currentNode==nodeChain.get(0))nodeChain.remove(currentNode);
        return Util.toPoint(x,y,nodeChain.get(0));

    }

    private double getTargetX(){
        if(targetPawn!=null)return targetPawn.getX();
        else return targetX;
    }

    private double getTargetY(){
        if(targetPawn!=null)return targetPawn.getY();
        else return targetY;
    }

    private void updatePathEnd(){
        //No need to do any of this if there's no target eelengine.physics.Pawn
        //check if target is in end node
        //if not, check if target is in node along path
        //if not, check if target is in neighbor of end node
    }
    public void draw(PGraphics p){
        //headTendril.draw(p);
        for(int i=0;i<nodeChain.size()-1;i++){
            Node node1=nodeChain.get(i);
            Node node2=nodeChain.get(i+1);
            p.stroke(0,255,0);
            p.line(Trans.x(node1.centerX()),Trans.y(node1.centerY()),Trans.x(node2.centerX()),Trans.y(node2.centerY()));
        }
        p.fill(0,255,0,100);
        p.ellipse(Trans.x(getTargetX()),Trans.y(getTargetY()),20,20);
    }
    public void print(){
        headTendril.print();
    }
}
