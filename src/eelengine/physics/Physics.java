package eelengine.physics;

/**
 * Created by Benjamin on 6/15/2017.
 * Shell for global physics variables.
 */
public class Physics {
    public static final int PLAYER=1;
    public static final int MOB=2;
    //public static final int FLYING_MOB=4;
    public static final int MAGIC=8;
    public static final int PROJECTILE=16;
    public static final int HALFWALL=32;
    public static final int WALL=64;
    public static final int LOW_PROP=128;

    public static final int M_WALLS =WALL|HALFWALL;
    public static final int M_HITS_CHARACTERS=PLAYER|MOB|MAGIC|PROJECTILE|HALFWALL|WALL;
    public static final int M_SOLID=PLAYER|MOB|WALL;
    public static final int M_ALL =Integer.MAX_VALUE;


}
