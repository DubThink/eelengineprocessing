package eelengine.physics;

import org.dyn4j.collision.CategoryFilter;
import org.dyn4j.collision.Filter;

/**
 * Created by Benjamin on 6/15/2017.
 * Collision filter class
 * Team 0 collides with any team
 * Team n will collide with anything other than team n
 */
public class TeamFilter extends CategoryFilter {
    private int team;
    public TeamFilter(long category, long mask) {
        this(0,category, mask);
    }
    public TeamFilter(int team, long category, long mask) {
        super(category, mask);
        this.team=team;
    }

    @Override
    public boolean isAllowed(Filter filter) {
        boolean teamStuff=true;
        if(filter instanceof TeamFilter&&team!=0)teamStuff=((TeamFilter)filter).team==0||((TeamFilter)filter).team!=team;
//        System.out.println("Testing "+this+" against "+filter+" team team test result: "+teamStuff);
        return teamStuff;//super.isAllowed(filter);
    }
    public String toString(){
        return "Team="+team+", category="+category+", mask="+mask;
    }
}
