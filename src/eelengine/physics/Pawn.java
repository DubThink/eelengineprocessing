package eelengine.physics;
/**
 * The interface for active @class gameObjects to interact with the physics engine.
 * Created by benja on 2/27/2017.
 */
import eelengine.GameObject;
import eelengine.Trans;
import org.dyn4j.collision.CategoryFilter;
import org.dyn4j.collision.Filter;
import org.dyn4j.dynamics.Body;
import org.dyn4j.dynamics.BodyFixture;
import org.dyn4j.dynamics.World;
import org.dyn4j.geometry.Circle;
import org.dyn4j.geometry.MassType;
import org.dyn4j.geometry.Vector2;
import processing.core.PGraphics;

public class Pawn {
    Body body;

    public Pawn(float x, float y){initBody(x,y);}
    public Pawn(float x, float y, TeamFilter filter) {
        this(x,y,filter,null);
    }
    public Pawn(float x, float y, TeamFilter filter, GameObject parent){
        initBody(x,y);
        setCollisionFilter(filter);
        body.setUserData(parent);

    }
    protected void initBody(float x, float y){
        body=new Body();
        Circle cirShape = new Circle(0.5);
        BodyFixture bodyFixture=new BodyFixture(cirShape);
        bodyFixture.setDensity(1.0D);
        bodyFixture.setFriction(2.0D);
        //bodyFixture.
        //bodyFixture.setFilter(new CategoryFilter(Physics.PLAYER,Physics.M_WALLS));
        {
            body.addFixture(bodyFixture);
            body.setMass(MassType.NORMAL);
            body.setLinearVelocity(new Vector2(0.0, 0.0));
            body.setAngularVelocity(0.0);
            body.setAutoSleepingEnabled(false);
            body.translate(x, y);
        }

    }
    public void draw(PGraphics p){
        System.out.println(getX()+" "+getY()+" "+getRot());
        p.pushMatrix();
        p.translate(Trans.x(getX()), Trans.y(getY()));
        p.rotate(Trans.r(getRot()));
        p.ellipse(0,0,10,10);
        p.line(0,0,0,10);
        p.popMatrix();
    }

    public void spawnInWorld(World world){
        world.addBody(body);
    }
    public void removeFromWorld(World world){
        world.removeBody(body);
    }

    public double getX(){
        return body.getTransform().getTranslationX();
    }

    public double getY(){
        return body.getTransform().getTranslationY();
    }

    public double getRot(){
        return body.getTransform().getRotation();
    }

    public Body getBody() {
        return body;
    }

    public Vector2 getPosition(){return body.getTransform().getTranslation();}
    public Vector2 getForwardVector(){
        return body.getLinearVelocity();
    }
    public double getForwardRotation(){
        return body.getLinearVelocity().getDirection();
    }
    public double getVelocity(){
        return body.getLinearVelocity().getMagnitude();
    }

    public void setCollisionFilter(Filter filter){
        body.getFixture(0).setFilter(filter);
    }
    public void setCollisionFilter(int category, int mask) {
        body.getFixture(0).setFilter(new CategoryFilter(category, mask));
    }
//    public void spawn(World world){
//        world.addBody(body);
//    }
    public void setParent(GameObject object){
        body.setUserData(object);
    }
}
