package eelengine.physics;

import eelengine.event.Event;
import org.dyn4j.dynamics.Body;
import org.dyn4j.dynamics.contact.ContactPoint;

/**
 * Created by Benjamin on 6/15/2017.
 * Implement this to receive callbacks on contact events from the physics engine.
 */
public interface ContactCallback {
    void onFirstContact(Body other, ContactPoint point);


    class ContactEvent implements Event{
        private ContactCallback callback;
        private Body other;
        private ContactPoint point;

        public ContactEvent(ContactCallback callback, Body other, ContactPoint point) {
            this.callback = callback;
            this.other = other;
            this.point = point;
        }

        @Override
        public void fire() {
            callback.onFirstContact(other,point);
        }
    }
}
