package eelengine.physics;

import org.dyn4j.geometry.Transform;
import org.dyn4j.geometry.Vector2;

/**
 * Created by Benjamin on 6/15/2017.
 * A pawn specifically for projectiles.
 */
public class ProjectilePawn extends Pawn {
    public ProjectilePawn(float x, float y, Vector2 velocity, int team) {
        super(x, y,new TeamFilter(team, Physics.PROJECTILE,Physics.M_SOLID));
        Transform startingTransform=body.getTransform();
        startingTransform.setRotation(velocity.getDirection());
        body.setTransform(startingTransform);
        body.applyForce(velocity);
    }

    @Override
    public void initBody(float x, float y) {
        super.initBody(x, y);
        body.setBullet(true);
    }
}
