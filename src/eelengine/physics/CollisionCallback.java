package eelengine.physics;

import eelengine.event.Event;
import org.dyn4j.dynamics.Body;

/**
 * Created by Benjamin on 6/15/2017.
 * Implement this to receive callbacks on collision events from the physics engine.
 */
public interface CollisionCallback {
    void onCollision(Body other);


    class CollisionEvent implements Event{
        private CollisionCallback callback;
        private Body other;

        public CollisionEvent(CollisionCallback callback, Body other) {
            this.callback = callback;
            this.other = other;
        }

        @Override
        public void fire() {
            callback.onCollision(other);
        }
    }
}
