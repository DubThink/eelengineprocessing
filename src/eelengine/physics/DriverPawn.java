package eelengine.physics;

import org.dyn4j.collision.CategoryFilter;
import org.dyn4j.dynamics.Body;
import org.dyn4j.dynamics.BodyFixture;
import org.dyn4j.dynamics.World;
import org.dyn4j.geometry.*;
import org.dyn4j.dynamics.joint.MotorJoint;

/**
 * Created by Benjamin on 5/26/2017.
 * A pawn that can move on its own
 * TODO enable toggling movement for knockback, etc
 */
public class DriverPawn extends Pawn{
    public Body controller;
    MotorJoint joint1;
    Vector2 averageForwardVector=new Vector2(1,0);
    float moveSpeed=.2f;
//    public eelengine.physics.DriverPawn(PApplet p, float x, float y){
//        super(p,x,y);
//    }
    public DriverPawn( float x, float y){
        super(x,y);
        this.initControl(x,y);
    }
    public DriverPawn(float x, float y, int team){
        super(x, y, new TeamFilter(team, Physics.MOB,Physics.M_HITS_CHARACTERS));
        this.initControl(x,y);
    }

    public void spawnInWorld(World world){
        world.addBody(body);
        world.addBody(controller);
        world.addJoint(joint1);
    }

    public void removeFromWorld(World world){
        world.removeBody(body);
        world.removeBody(controller);
        world.removeJoint(joint1);
    }

    protected void initBody(float x, float y){
        super.initBody(x,y);
        setCollisionFilter(new CategoryFilter(Physics.PLAYER,Physics.M_HITS_CHARACTERS));
//        body=new Body();
//        Circle cirShape = new Circle(0.5);
//        BodyFixture bodyFixture=new BodyFixture(cirShape);
//        bodyFixture.setDensity(1.0D);
//        bodyFixture.setFriction(2.0D);
//        bodyFixture.setFilter(new CategoryFilter(Physics.PLAYER,Physics.M_WALLS));
//        {
//            body.addFixture(bodyFixture);
//            body.setMass(MassType.NORMAL);
//            body.setLinearVelocity(new Vector2(0.0, 0.0));
//            body.setAngularVelocity(0.0);
//            body.setAutoSleepingEnabled(false);
//            body.translate(x, y);
//        }
    }
    private void initControl(float x, float y){
        controller=new Body();
        Convex cirShape = Geometry.createSquare(0.5);
        {
            BodyFixture fixture=new BodyFixture(cirShape);
            fixture.setSensor(true);
            controller.addFixture(fixture);
        }
        controller.setAngularVelocity(0.0);
        controller.setMass(MassType.INFINITE);
        controller.translate(x, y);

        joint1 = new MotorJoint(controller, body);
        joint1.setLinearTarget(new Vector2(0, 0));
        joint1.setAngularTarget(0);
        joint1.setCorrectionFactor(0.3);
        // allow translational changes (change this depending on how fast you
        // want the playerPawn body to react to changes in the controller body)
        joint1.setMaximumForce(1000);
        // allow rotational changes (change this depending on how fast you want
        // the playerPawn body to react to changes in the controller body)
        joint1.setMaximumTorque(7.0);
        joint1.setCollisionAllowed(false);
    }
    public void moveto(float x, float y) {
        Vector2 target=new Vector2(x,y);
        double angle = this.controller.getTransform().getRotation();
        Transform tx = new Transform();
        tx.translate(target);
        tx.rotate(angle, target);
        this.controller.setTransform(tx);
        //p.println(joint1);
    }

    /**
     *
     * @param x -1.0 to 1.0
     * @param y -1.0 to 1.0
     */
    public void setMoveVector(float x, float y){
        setMoveVector(new  Vector2(x,y));

    }

    public void setMoveVector(Vector2 targetV) {
        if(targetV.getMagnitude()>1)targetV.normalize();
        targetV=targetV.multiply(moveSpeed);
        Vector2 target=this.body.getTransform().getTranslation().add(targetV);//new Vector2(x,y);
        averageForwardVector=averageForwardVector.multiply(0.9).add(targetV.multiply(0.1));
        double angle = this.controller.getTransform().getRotation();
        Transform tx = new Transform();
        tx.translate(target);
        tx.rotate(angle, target);
        this.controller.setTransform(tx);
    }

        @Override
    public double getForwardRotation() {
        return averageForwardVector.getDirection();
    }
}
