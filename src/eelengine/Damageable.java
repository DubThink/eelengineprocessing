package eelengine;

/**
 * Created by Benjamin on 6/16/2017.
 * Classes that implement this can receive damage
 */
public interface Damageable {
    int STANDARD=1;
    void addDamage(int amount, int damageType);
}
