package eelengine;

import ddf.minim.AudioPlayer;
import ddf.minim.AudioSample;
import ddf.minim.Minim;
import processing.core.PApplet;

import java.io.File;
import java.util.HashMap;

/**
 * Created by Benjamin on 6/17/2017.
 * A manager class for sound using the minim library
 * cleanup() must be called at program exit.
 */
public class SoundManager {
    public static boolean verboseMode=true;
    private static Minim minim;
    private static AudioPlayer audioPlayer;
    private static HashMap<String,AudioSample> sampleLibrary=new HashMap();
    private static boolean init=false;
    public static void initialize(PApplet p){
        if(init)return;
        minim=new Minim(p);
        init=true;
    }
    public static void loadSamples(String ... fileNames){
        for(String fileName:fileNames){
            String name=new File(fileName).getName();
            if(!new File(fileName).exists()){
                System.err.println("(SoundManager)Cannot load \""+fileName+"\": file does not exist.");
                return;
            }
//            if(!name.matches("\\w+") ){
//                System.err.println("(SoundManager) No extension \""+fileName+"\""); continue;
//            }
            if(sampleLibrary.containsKey(name)){
                System.err.println("(SoundManager) Sample \""+name+"\" already exists.");
            }
            AudioSample sample = minim.loadSample(fileName);
            if(sample==null) {
                System.err.println("(SoundManager)Cannot load \""+fileName+"\""); continue;
            }
            if(verboseMode)System.out.println("Loaded \""+name+"\"");
            sampleLibrary.put(name,sample);
        }
    }
    public static void playSample(String name){
        if(sampleLibrary.containsKey(name))sampleLibrary.get(name).trigger();else System.err.println("(SoundManager) No sample \""+name+"\"");
    }
    public static void cleanup(){
        for(AudioSample sample:sampleLibrary.values())sample.close();
    }
}
