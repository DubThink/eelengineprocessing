import eelengine.Util;
import processing.core.PApplet;

/**
 * Created by benja on 4/13/2017.
 */
public class Button {
    public float x,y,w,h;
    public boolean on;
    public boolean toggle;
    public String label;
    private boolean lastPressed=false;
    private PApplet p;

    public Button(PApplet p, float x, float y, float w, float h, String label) {
        this(p,x,y,w,h,label,false);
    }
    public Button(PApplet p, float x, float y, float w, float h, String label, boolean toggle) {
        this.p=p;
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
        this.label = label;
        this.toggle = toggle;
    }
    public void check(){
        boolean overButton=checkIfOver();
        boolean pressed=p.mousePressed&&p.mouseButton==p.LEFT;
        if(toggle){
            if(overButton&&!lastPressed&&pressed)on=!on;
        }else {
            if(overButton&&pressed)on=true;
            else on=false;
        }
        lastPressed=p.mousePressed;
    }
    public boolean checkIfOver(){
        return Util.in(p.mouseX,x,x+w)&& Util.in(p.mouseY,y,y+h);
    }
    public void draw(){
        int alpha =127;// checkIfOver()?127:100;
        if(on) p.fill(60,60,60,alpha);
        else p.fill(30,30,30,alpha);
        p.strokeWeight(3);
        p.stroke(80,80,80);
        p.rect(x,y,w,h,3);
        if(checkIfOver()) p.fill(100,20,20);
        else p.fill(80,80,80);
        p.textAlign(p.CENTER,p.CENTER);
        p.textSize(24);
        p.text(label,x+w/2,y+h/2);
        p.textAlign(p.LEFT);
    }
}
