/**
 * Created by benja on 2/27/2017.
 * use http://magiccards.info
 */
import ddf.minim.Minim;
import eelengine.Engine;
import eelengine.SoundManager;
import eelengine.TurboBar;
import processing.core.PApplet;
import processing.core.PFont;
import processing.event.MouseEvent;
import processing.opengl.PJOGL;

public class EelGame extends PApplet{

    GameController gc;

    //Button button=new Button(this,20,20,180,50,"lolol");

    public void settings(){
        // I believe:
        // negative is primary monitor
        // 0 is all monitors (woo!)
        // positive is specific monitor, no idea how it's decided. 1 isn't always primary
        if(false)fullScreen(P2D,-2);
        else
        {
            size(1800,900,P2D);
        }
        smooth(8);
        //noSmooth();
        //surface.setIcon(icon);
        PJOGL.setIcon("data/BlueLogo2(007fff).png");
    }

    public void  setup(){
        System.out.println(
                        "    ______     ________            _          \n" +
                        "   / ____/__  / / ____/___  ____ _(_)___  ___ \n" +
                        "  / __/ / _ \\/ / __/ / __ \\/ __ `/ / __ \\/ _ \\\n" +
                        " / /___/  __/ / /___/ / / / /_/ / / / / /  __/\n" +
                        "/_____/\\___/_/_____/_/ /_/\\__, /_/_/ /_/\\___/ \n" +
                        "© 2017 Benjamin Welsh    /____/     \n");
        //System.out.println("\n");
        System.out.println("EelGame Initializing...");
        surface.setTitle("EelGame");
        PFont font=createFont("data/segoeui.ttf",36);
        textFont(font);
        gc =new GameController(this);
        surface.setResizable(true);
        Engine.setup();
        System.out.println("EelGame Initialized");
        frameRate(60);
        SoundManager.initialize(this);
        SoundManager.loadSamples("data\\sound\\eelboop1.wav");
        SoundManager.playSample("eelboop1.wav");
        TurboBar.ebar=loadImage("data/ebar.png");
    }

    public void draw(){
        gc.tick();
    }

    public void mousePressed(){
        gc.mousePressed();
    }
    public void mouseMoved(){

        //if(mouseButton==RIGHT)gc.model.hand.kill(gc.model.selected);
    }
    public void mouseWheel(MouseEvent event) {
        int e = event.getCount();
        gc.mouseWheel(e);
    }
    public void keyPressed(){
        gc.keyBinder.keyDown(keyCode);
        //println(keyCode,key==CODED);
        //Movement
        if(keyCode==SHIFT)gc.shift=true;
        if(key=='w'||key=='W')gc.addMove(1,0);
        else if(key=='s'||key=='S')gc.addMove(-1,0);
        else if(key=='a'||key=='A')gc.addMove(0,-1);
        else if(key=='d'||key=='D')gc.addMove(0,1);


        if(keyCode==100){
            gc.debugPanel=!gc.debugPanel;
        }else if(keyCode==99){
            gc.devView=!gc.devView;
        }else if(keyCode==98){
            gc.showPhysics=!gc.showPhysics;
        }
        //F5
        else if(keyCode==101){
            gc.gameWorld.saveNav();
        }else if(keyCode==102){
            gc.toggleNavEdit();
        }else if(keyCode==103){
            gc.gameWorld.navigation.buildNeighbors();
        }else if(keyCode==104){
            gc.reloadNav();
        }
        //F9
        else if(keyCode==105){
            gc.spook.setTargetPawn(gc.playerPawn);
        }else if(keyCode==106){
            gc.toggleWorldEdit();
        }else if(keyCode==107){
            gc.gameWorld.addWall(-1,4,5,7,false);
        }else if(keyCode==108){
            gc.testProjectile();
            //SoundManager.playSample("test_multi_beep_2s.wav");
        }
//        else if (key == '[') {
//            gc.model.swapPlayers();
//        }else if (key == ']') {
//            gc.model.theirField.add(gc.model.selected);
//        }
//        if (key == 27){
//            key=0;
//            actionEscape();
//        }

    }
    public void keyReleased(){
        gc.keyBinder.keyUp(keyCode);
        if(keyCode==SHIFT)gc.shift=false;

        if(key=='w'||key=='W')gc.addMove(-1,0);
        else if(key=='s'||key=='S')gc.addMove(1,0);
        else if(key=='a'||key=='A')gc.addMove(0,1);
        else if(key=='d'||key=='D')gc.addMove(0,-1);

    }
    public void mouseReleased(){
        gc.mouseReleased();
    }
//    private void promptKeyInput(){
//        if(keyCode==BACKSPACE){
//            if(inputBuffer.length()>0)inputBuffer=inputBuffer.substring(0,inputBuffer.length()-1);
//        }else if(key=='\t'&&promptQuery==null){
//            promptMode=false;
//        }else if(key!=CODED&&(key+"").matches("[^|\\n\\r]")){
//            inputBuffer=inputBuffer+key;
//        }else if(key=='\n'){
//            promptMode=false;
//            handlePrompt();
//        }
//    }
    @Override
    public void exit() {
        System.out.println("EXITING");
        Engine.cleanup();
        super.exit();
    }
    public static void main(String... args){
        PApplet.main("EelGame");
    }
}
